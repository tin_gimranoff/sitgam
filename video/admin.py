from django.contrib import admin
from django.utils.html import format_html

from video.models import *

class VideoDontWorkAdmin(admin.ModelAdmin):
    class Media:
        js = (
            'js/custom_admin.js',
        )
    list_display = ('name', 'reason', 'add_reason', 'link_url', 'isRead')
    readonly_fields = ('link_url',)
    fields = ('name', 'reason', 'add_reason', 'isRead', 'link_url')

    def link_url(self, instance):
        return format_html(
            '<a href="{0}" target="_blank">{1}</a>',
            instance.url,
            instance.url,
        )
    link_url.short_description = "Сылка на страницу с видео"
    link_url.allow_tags = True
    link_url.mark_safe = True


class VideoAdmin(admin.ModelAdmin):
    list_display = ('name', 'fighter_1', 'fighter_2', 'fight', 'event', 'owner', 'link_url', 'isModerated')
    readonly_fields = ('link_url',)
    fields = ('name', 'fighter_1', 'fighter_2', 'fight', 'event', 'link', 'owner', 'isModerated', 'link_url')

    def link_url(self, instance):
        return format_html(
            '<a href="{0}" target="_blank">{1}</a>',
            instance.url,
            instance.url,
        )
    link_url.short_description = "Сылка на страницу с видео"
    link_url.allow_tags = True
    link_url.mark_safe = True


admin.site.register(Video, VideoAdmin)
admin.site.register(VideoDontWork, VideoDontWorkAdmin)
