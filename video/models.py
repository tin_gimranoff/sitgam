from django.db import models

from fighting.models import Fighter, Fight, Event


class Video(models.Model):

    def __str__(self):
        return self.name if type(self.name) is str else 'Видео'

    class Meta:
        db_table = 'videos'
        verbose_name = u'Видео'
        verbose_name_plural = u'Видео'

    name = models.CharField(max_length=255, verbose_name=u'Название видео', blank=True, null=True)
    fighter_1 = models.ForeignKey(Fighter, verbose_name=u'Боец 1', null=True, blank=True, on_delete=models.SET_NULL, related_name='fighter_video_1')
    fighter_2 = models.ForeignKey(Fighter, verbose_name=u'Боец 2', null=True, blank=True, on_delete=models.SET_NULL, related_name='fighter_video_2')
    fight = models.ForeignKey(Fight, verbose_name=u'Бой', null=True, blank=True, on_delete=models.SET_NULL)
    event = models.ForeignKey(Event, verbose_name=u'Событие', null=True, blank=True, on_delete=models.SET_NULL, related_name='videos')
    link = models.CharField(max_length=255, verbose_name=u'Ссылка на видео', blank=False, null=False, help_text=u'Пример корректной ссылки: https://www.youtube.com/watch?v=WdodEsDwTIA')
    owner = models.CharField(max_length=255, verbose_name=u'Кто добавил', blank=True, null=True)
    isModerated = models.BooleanField(verbose_name=u'Прошло модерацию', default=True)
    url = models.CharField(max_length=255, verbose_name=u'URL на страницу с видео', blank=True, null=True)


class VideoDontWork(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'videos_dont_work'
        verbose_name = u'Жалоба на видео'
        verbose_name_plural = u'Жалобы на видео'

    url = models.CharField(max_length=255, verbose_name=u'URL страницы с видео', blank=False, null=False)
    name = models.CharField(max_length=255, verbose_name=u'Название видео', blank=True, null=True)
    reason = models.CharField(max_length=255, verbose_name=u'Причина обращения', blank=False, null=False)
    add_reason = models.CharField(max_length=255, verbose_name=u'Дополнительное описанние', blank=True, null=True)
    isRead = models.BooleanField(verbose_name=u'Прочитано', default=False)
