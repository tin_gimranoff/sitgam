import re

from django import template
from urllib import parse

register = template.Library()


@register.inclusion_tag('_video_template_frame.html')
def video_frame(link):
    if re.match(r'^<iframe.*</iframe>$', link, flags=re.IGNORECASE):
        return {
            'type': 'frame',
            'link': link,
        }
    if re.match(r'^https:\/\/www.youtube.com/watch\?v=[0-9a-zA-Z]+', link, flags=re.IGNORECASE):
        return {
            'type': 'youtube_link',
            'link': "https://www.youtube.com/embed/%s" % parse.parse_qs(parse.urlsplit(link).query)['v'][0],
        }
