import re

from django import forms

from fighting.models import Fighter, Fight, Event
from video.models import VideoDontWork, Video


class VideoDontWorkForm(forms.ModelForm):
    class Meta:
        model = VideoDontWork
        fields = "__all__"


class AddVideoForm(forms.ModelForm):

    object_type = forms.CharField(required=True)
    object_id = forms.IntegerField(required=True)

    class Meta:
        model = Video
        fields = "__all__"

    def clean(self, *args, **kwargs):
        super(AddVideoForm, self).clean(*args, **kwargs)
        data = self.cleaned_data
        if data['object_type'] == 'fighter':
            data['fighter_1'] = Fighter.objects.get(id=data['object_id'])
        if data['object_type'] == 'fight':
            data['fight'] = Fight.objects.get(id=data['object_id'])
        if data['object_type'] == 'event':
            data['event'] = Event.objects.get(id=data['object_id'])
        data['isModerated'] = True
        return data

    def clean_link(self):
        link = self.cleaned_data['link']
        if not re.match(r'^<iframe.*</iframe>$', link, flags=re.IGNORECASE) and not re.match(r'^https:\/\/www.youtube.com/watch\?v=[0-9a-zA-Z]+', link, flags=re.IGNORECASE):
            self.add_error('link', 'Формат ссылки не верный. Используйте или YouTube ссылку или iframe')
        return link

    def save(self, commit=True):
        instance = super(AddVideoForm, self).save(commit=True)
        if not re.match(r'^http(s|):\/\/.+\#[0-9]+_v$', instance.url, flags=re.IGNORECASE):
            instance.url = instance.url[0:len(instance.url)-1] + '#' + str(instance.id) + '_v'
            instance.save()
