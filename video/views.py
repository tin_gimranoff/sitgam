from urllib.parse import unquote

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from video.forms import VideoDontWorkForm, AddVideoForm


@csrf_exempt
def dont_work(request):
    form = VideoDontWorkForm(request.POST)
    if form.is_valid():
        form.save()
        return JsonResponse({'status': True}, safe=False)
    else:
        return JsonResponse({'status': False, 'errors': form.errors}, safe=False)

@csrf_exempt
def add_video(request):
    form = AddVideoForm(request.POST)
    if form.is_valid():
        form.save()
        return JsonResponse({'status': True}, safe=False)
    else:
        return JsonResponse({'status': False, 'errors': form.errors}, safe=False)
