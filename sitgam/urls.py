"""sitgam URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path

from fighting import views as fighting_views
from settings import views as settings_views
from mainpage import views as mainpage_views
from news import views as news_views
from textpages import views as textpages_views
from rates import views as rates_views
from search import views as search_views
from video import views as video_views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('jet/', include('grappelli.urls')),  # grappelli URLSs
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('', mainpage_views.index),
    path('fighter/<str:alias>/', fighting_views.fighter_details),
    path('fight/<str:alias>/', fighting_views.fight_details),
    path('calendar/', fighting_views.calendar),
    path('calendar/<str:alias>/', fighting_views.calendar),
    path('event/<str:alias>/', fighting_views.event_details),
    path('fights/', fighting_views.fights),
    path('fights/<str:alias>/', fighting_views.fights),
    # news pages
    path('news/', news_views.list),
    path('news/category/<str:alias>/', news_views.list),
    path('news/tag/<str:tag>/', news_views.list),
    path('news/<str:alias>/', news_views.details),
    # ajax for autocomplete
    path('ajax/get_fighters_by_name/', fighting_views.get_fighters_by_name),
    path('ajax/get_events_by_name/', fighting_views.get_events_by_name),
    path('ajax/get_promotions_by_name/', fighting_views.get_promotions_by_name),
    # ajax for filter on mainpage
    path('ajax/get_last_fights/', mainpage_views.get_last_fights),
    path('ajax/get_last_videos/', mainpage_views.get_last_videos),
    path('ajax/get_last_news/', mainpage_views.get_last_news),
    # ajax for right panel (RP)
    path('ajax/get_rp_fighter_rate/', mainpage_views.get_rp_fighter_rate),
    path('ajax/get_rp_waiting_fights/', mainpage_views.get_rp_waiting_fights),
    path('ajax/get_rp_best_events/', mainpage_views.get_rp_best_events),
    path('ajax/get_rp_best_fights/', mainpage_views.get_rp_best_fights),
    # ajax for get and set human rate
    path('ajax/set_human_mark/', fighting_views.set_human_mark),
    path('ajax/get_human_mark/', fighting_views.get_human_mark),
    # ajax admin panel
    path('settings/get_weight_category_by_sport/', settings_views.get_weight_category_by_sport),
    # ajax for sending dont work video form
    path('video/dont_work/', video_views.dont_work),
    path('video/add_video/', video_views.add_video),
    # search page
    path('search/', search_views.search),
    # rates pages
    re_path(r'^rate/$', rates_views.detect_first_rate_and_redirect),
    re_path(r'^rate/(?P<sport>[\w-]+)/(?P<type>(fighters|events|fights))/(?P<rate>(human|system))/$', rates_views.rate),
    # all textpages if page not exist rediret to 404
    path('<str:alias>/', textpages_views.show_content),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'errors.views.error_404_view'
