var $ = jQuery;
$(document).ready(function(){
    // Ховер на панель рейтинга
    $(".human-rating-panel").bind('mousemove', function(e) {
        $(this).css('cursor', 'pointer')
        var offset = $(this).offset()
        x = e.pageX-offset.left
        $(".human-rating-panel div").css('width', x+'%');
    })
    // Событие когда убираем мышку, возвращаем рейтинг в позицию среднего
    $(".human-rating-panel").bind('mouseout', function() {
        var fight_id = $(this).attr('data-id')
        $.ajax({
            url: '/ajax/get_human_mark/',
            method: 'post',
            data: 'fight_id='+fight_id,
            dataType: 'json',
            success: function (data) {
                set_human_mark_on_panel(data)
            }
        });
        $(".human-rating-panel div").css('width', get_human_rate()+'%');
    })
    // Клик по звездочкам
    $(".human-rating-panel").bind('click', function(e){
        var fight_id = $(this).attr('data-id')
        var offset = $(this).offset()
        x = e.pageX-offset.left
        $.ajax({
            url: '/ajax/set_human_mark/',
            method: 'post',
            data: 'fight_id='+fight_id+'&mark='+x,
            dataType: 'json',
            success: function (data) {
                $(".human-rating-panel").css('cursor', 'auto')
                $(".human-rating-panel").off('mousemove')
                $(".human-rating-panel").off('mouseout')
                $(".human-rating-panel").off('click')
                set_human_mark_on_panel(data)
            }
        });

    })
})
// Функция уставнноки рейтинга в панели
function set_human_mark_on_panel(data) {
    $(".new_mark").empty().append(data.point)
    $(".count_marks").empty().append(data.count)
    $(".human-rating-panel div").css('width', data.point_in_percents+'%');
}

// Фильтр рейтинга бойцов, кнопки муж и жен
function get_human_rate() {
    return 60;
}