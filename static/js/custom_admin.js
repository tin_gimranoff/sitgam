jQuery(document).ready(function () {

    var id_winner_val = null;
    if(jQuery("#id_winner").val() != '') {
        id_winner_val = jQuery("#id_winner").val();
    }

    jQuery("#id_winner").empty();
    var newOption = new Option('---------', '', false, false);
    jQuery('#id_winner').append(newOption);
    if(jQuery("#id_fighter_1").val() != '') {
        var fighter_1_id = jQuery("#id_fighter_1 option:selected").val();
        var fighter_1_name = jQuery("#id_fighter_1 option:selected").text();
        var newOption = new Option(fighter_1_name, fighter_1_id, false, false);
        jQuery('#id_winner').append(newOption);
    }

    if(jQuery("#id_fighter_2").val() != '') {
        var fighter_2_id = jQuery("#id_fighter_2 option:selected").val();
        var fighter_2_name = jQuery("#id_fighter_2 option:selected").text();
        var newOption = new Option(fighter_2_name, fighter_2_id, false, false);
        jQuery('#id_winner').append(newOption);
    }

    if(id_winner_val != null) {
        jQuery("#id_winner").val(id_winner_val).change();
    }

    jQuery("#id_kind_of_fight").bind('change', function () {
        var sport_value = jQuery(this).val();
        jQuery.ajax({
            url: '/settings/get_weight_category_by_sport/',
            type: 'post',
            data: { sport_id: sport_value},
            dataType: 'json',
            success: function (data) {
                jQuery("#id_weight_category").empty();
                jQuery.each(data, function (i, item) {
                    jQuery("#id_weight_category").append('<option value="'+item.id+'">'+item.name+'</option>');
                })
            }
        })
    });

    jQuery("#id_fighter_1, #id_fighter_2").bind('select2:select', function(){
        var fighter_1_id = jQuery("#id_fighter_1 option:selected").val();
        var fighter_1_name = jQuery("#id_fighter_1 option:selected").text();
        var fighter_2_id = jQuery("#id_fighter_2 option:selected").val();
        var fighter_2_name = jQuery("#id_fighter_2 option:selected").text();
        jQuery("#id_winner").empty();
        var newOption = new Option('---------', '', false, false);
        jQuery('#id_winner').append(newOption);
        if(fighter_1_id != '') {
            var newOption = new Option(fighter_1_name, fighter_1_id, false, false);
            jQuery('#id_winner').append(newOption);
        }
        if(fighter_2_id != '') {
            var newOption = new Option(fighter_2_name, fighter_2_id, false, false);
            jQuery('#id_winner').append(newOption);
        }
    });

    jQuery("#id_fighter_1_titles").bind('select2:select', function () {
        jQuery("#id_fighter_2_titles").val($(this).val()).trigger('change');
    });

    jQuery("#id_isRead").prop( "checked", true );
});