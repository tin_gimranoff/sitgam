$(document).ready(function() {

    _events_block_sport_lables();

    $(".waiting-fights-list .waiting-fight-link").hover(
        function() {
            $(this).removeClass('white-bg').addClass('blue-bg');
        }, function() {
            $(this).removeClass('blue-bg').addClass('white-bg');
        }
    );

    $(window).scroll(function() {
        _fixed_menu($(window).width(), $(window).scrollTop(), $(".main-menu.hidden-lg.hidden-md.hidden-sm").position().top);
    });

    $(window).resize(function() {
        if ($(window).width() < 1200)
            $(".input-date").css('width', parseInt($("input[name='name']").outerWidth() - $(".input-group-addon").outerWidth()));
        else
            $(".input-date").css('width', '100%');

        _events_block_sport_lables();
    });

    // Обрабатываем куку показывать результаты
    if($.cookie('status_result') == 'true') {
        var checkbox = $('input[name=status_showing_results]');
        $(".rs").show();
        if(checkbox.prop('checked') == false)
            $(checkbox).click();
    } else {
        $(".rs").hide();
    }

    $(".last-fights-mobile-item").click(function(e) {
        e.preventDefault();
        $(this).parent().children(".last-fights-mobile-item-full-info").slideToggle();
    });

    $("#event-form-rate").slider({id: 'event-form-date-slider', min: 1, max: 5, range: true, value: [2, 4], step: 0.1});
    $("#user-form-rate").slider({id: 'user-form-date-slider', min: 1, max: 5, range: true, value: [2, 4], step: 0.1});

    if(typeof $().datetimepicker === "function") {
        $(".input-date").datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY',
        });
    }

    $("input[name=status_showing_results]").click(function() {
        if ($(this).is(':checked'))
            $(".fight-result").show();
        else
            $(".fight-result").hide();
    });

    $("input[name=gender]").click(function() {
        if ($(this).val() == 'future') {
            $(".user-rating").hide();
        } else {
            $(".user-rating").show();
        }
    });

    // Последние 5 боев обработка ховера на строку
    $(".last-six-fights td").hover(
        function() {
            var parent = $(this).parent();
            var video_link = parent.attr('data-link');
            var video_icon = parent.attr('data-icon');
            parent.css({'background': '#002460'});
            parent.find('td:first-child').css({'color': '#ebba16'});
            parent.find('td:nth-child(2)').find('div:first-child').css({'color': '#ebba16'});

            parent.find('td:nth-child(3)').find('div').hide();
            parent.find('td:nth-child(4)').find('div').hide();

            parent.find('td:nth-child(3)').css({
                'padding': '0',
                'vertical-align': 'middle'
            }).append("<div class='p-v text-center'><a target='_blank' href='" + video_link + "' style='color: #ebba16; font-size: 12px'><img src='"+video_icon+"'><br />Посмотреть бой</a></div>");
            parent.find('td:nth-child(4)').append("<div class='p-v text-center'><a target='_blank' href='" + video_link + "'><img src='"+video_icon+"'></a></div>");
        }, function() {
            var parent = $(this).parent();
            parent.css({'background': 'white'});
            parent.find('td:first-child').css({'color': 'black'});
            parent.find('td:nth-child(2)').find('div:first-child').css({'color': 'black'});

            parent.find('td:nth-child(3)').css({'padding': '8px', 'vertical-align': 'middle'})
            parent.find('td:nth-child(3)').find('div').show();
            parent.find('td:nth-child(4)').find('div').show();

            $('.p-v').remove();
        }
    );

    $(".result-spoiler-btn").click(function() {
        $(".result-spoiler").slideToggle();
    });

    $(".best-fight-rate-container").hover(function() {
        $(this).css('background', '#002563');
        $(this).find(".fighter-rate-page-name").css('color', 'white');
    }, function() {
        $(this).css('background', 'white');
        $(this).find(".fighter-rate-page-name").css('color', 'black');
    });

    $(".event-fight-info-tbl").hover(function() {
        $(this).find('td').css('background', '#002460');
        $(this).removeClass('ft-black').addClass("ft-white");
        $(this).find(".fighter-name").css({'color': '#fcb40b'});
        $(this).find(".horiz-vs-devider span").css('color', 'black');
    }, function() {
        $(this).find('td').css('background', '#f3f3f5');
        $(this).removeClass('ft-white').addClass("ft-black");
        $(this).find(".fighter-name").css({'color': 'black'});
        $(this).find(".horiz-vs-devider span").css('color', 'black');
    });

    $(".show-results-checkbox-event-page input").click(function() {
        if ($(this).is(':checked'))
            $(".rs").show();
        else
            $(".rs").hide();
    });

    $(".text-legend span").hover(function() {
        $(".text-legend-popup img").attr('src', $(this).attr('data-opponent-img'))
        $(".text-legend-popup-description").empty().append($(this).attr('data-opponent-name') + "<br />" + $(this).attr('data-winner-comment'));
        $(".text-legend-popup").show();
    }, function() {
        $(".text-legend-popup").hide();
    });

    var wins_block_height = $(".profile-fight-statistic-blocks .wins-block").innerHeight();
    var loose_block_height = $(".profile-fight-statistic-blocks .lose-block").innerHeight();
    var noany_block_height = $(".profile-fight-statistic-blocks .noany-block").innerHeight();

    var max_block_height = Math.max(wins_block_height, loose_block_height, noany_block_height);
    $(".profile-fight-statistic-blocks .wins-block, .profile-fight-statistic-blocks .lose-block, .profile-fight-statistic-blocks .noany-block").css('height', max_block_height);

    var last_battle_items = $(".last-battle-item");
    var max_last_battle_item_height = 0;
    $.each(last_battle_items, function(i, item) {
        if ($(item).outerHeight() > max_last_battle_item_height)
            max_last_battle_item_height = $(item).outerHeight();
    });
    $(".last-battle-item").css('height', max_last_battle_item_height + 15);

    if(typeof $().typeahead === "function") {
        // AJAX for fighters autocomplete
        $('.find-fight-page-form input[name=fighter_1], .find-fight-page-form input[name=fighter_2], .find-fight-page-form input[name=fighter_2_full]').typeahead({
            matcher: function(item) {
                  return true;
            },
            source: function(query, result) {
                $.ajax({
                    url: "/ajax/get_fighters_by_name/",
                    data: 'q=' + query,
                    dataType: "json",
                    type: "GET",
                    success: function(data) {
                        result($.map(data, function (item) {
                            return item;
                        }));
                    }
                });
            }
        });

        // AJAX for events autocomplete
        $('.find-fight-page-form input[name=event]').typeahead({
            source: function(query, result) {
                $.ajax({
                    url: "/ajax/get_events_by_name/",
                    data: 'q=' + query,
                    dataType: "json",
                    type: "GET",
                    success: function(data) {
                        result($.map(data, function (item) {
                            return item;
                        }));
                    }
                });
            }
        });

        // AJAX for promouter autocomplete
        $('.find-fight-page-form input[name=promotion]').typeahead({
            source: function(query, result) {
                $.ajax({
                    url: "/ajax/get_promotions_by_name/",
                    data: 'q=' + query,
                    dataType: "json",
                    type: "GET",
                    success: function(data) {
                        result($.map(data, function(item) {
                            return item;
                        }));
                    }
                });
            }
        });
    }

    // Connect two gender fields in mobile and desktop version in fights filter
    $("input[name=gender]").click(function() {
        var val = $(this).val();
        $("input[name=gender_2][value='"+val+"']").attr('checked', 'checked');
    });
    $("input[name=gender_2]").click(function() {
        var val = $(this).val();
        $("input[name=gender][value='"+val+"']").attr('checked', 'checked');
    });

    // AJAX FILTER
    $(".filter-tabs[data-type=ajax] li").bind('click', function() {
        $(this).parent('ul').children('li').removeClass('active');
        $(this).addClass('active');
        var action = $(this).parent('ul').attr('data-action');
        var param = $(this).attr('data-param');
        var block = $(this).parent('ul').attr('data-block');
        var pager_block = $(this).parent('ul').attr('data-pager');
        // Если block у нас - рейтинг бойцов то надо скинуть кнопки муж и жен
        if(block == '.fighter-rate-list') {
            $(".fighter-rate-block-gender-filter li").removeClass('active');
        }
        $.ajax({
            url: action,
            method: 'post',
            data: 'param=' + param,
            dataType: 'json',
            success: function(data) {
                $(block).empty().html(data.html);
                if(typeof pager_block != "undefined") {
                    $(pager_block).empty().html(data.pager);
                    rebind_news_mainpage_pagination();
                }
            }
        });
    });

    // Клик на кнопки МУЖ и ЖЕН в рейтиге бойцов на главнойй
    $(".fighter-rate-block-gender-filter li").bind('click', function() {
        $(".fighter-rate-block-gender-filter li").removeClass('active');
        $(this).addClass('active');
        rp_filter_fighters();
        return false;
    });

    // Меню с весовыми категориями в рейтинге бойцов
    $(".fighter-rate-block-gender-filter li a").bind('click', function() {
        var block_selector = ".fighter-rate-block-weight-categories-popup";
        $(block_selector).css('min-width', $(this).parent().outerWidth()).css('left', $(this).parent().position().left);
        if($(block_selector).css('display') == 'none')
            $(block_selector).css('display', 'inline-block')
        else
            $(block_selector).css('display', 'none');
    });

    // Выбор весовой категории в рейтинге бойцлв
    $(".fighter-rate-block-weight-categories-popup a").bind('click', function() {
        var weight_category_id = $(this).parent().attr('data-id');
        var weight_category_name = $(this).text();
        $("input[name=rp_rate_weight_category]").val(weight_category_id);
        $(".fighter-rate-block-weight-filter-info span").empty().append(weight_category_name);
        $(".fighter-rate-block-weight-categories-popup").css('display', 'none');
        rp_filter_fighters();
        return false;
    });

    // Крестик когда выбрана весовая категория скидываем фильтр
    $(".fighter-rate-block-weight-filter-info a").bind('click', function() {
        $("input[name=rp_rate_weight_category]").val('');
        $(".fighter-rate-block-weight-filter-info span").empty().append('Независимо от веса');
        rp_filter_fighters();
       return false;
    });

    $(window).bind('click', function() {
        if($(".fighter-rate-block-weight-categories-popup").css('display') != 'none')
            $(".fighter-rate-block-weight-categories-popup").css('display', 'none');
    });

    //NEWS ON MAIN PAGE PAGINATION
    rebind_news_mainpage_pagination();

    // Поиск на мобильной версии сайта
    $(".header-search-container-mobile .fa-search").click(function (){
        $(".pluso").toggle();
        $(".mobile-search-form").toggle();
        return false;
    });

    // Меню мобильной версии сайта
    $(".mobile-menu-icon").click(function(){
        $(".mobile-menu-container").toggle(500);
    });

    // Фунция для обеспечения правильного перехода по ссылкам в блоке с последними 6-ю боями
    $(document).on('click', '[data-onclick-url], [data-onclick-url] a', function(e){
        if($(this).prop('tagName') == "A") {
            e.stopPropagation();
        } else {
            var url = $(this).attr('data-onclick-url');
            window.open(url, '_blank');
            //window.location.href = url;
            return false;
        }
    });

    // Открытие формы для отправки заявки где не работает видео
    $('#video_dont_work').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var pageUrl = button.data('page-url');
        var videoName = button.data('video-name');
        var modal = $(this);
        modal.find('input[name="url"]').val(pageUrl);
        modal.find('input[name="name"]').val(videoName);
        modal.find('.alert').remove();
    });

    // Если мы выбрали в форме "Не работает видео"  - другое
    // Надо показать текстовое поле, которое по умолчанию скрыто
    $("#video_dont_work select[name='reason']").change(function () {
        if($(this).val() == 'Другое') {
            $("#video_dont_work textarea[name='add_reason']").show();
        } else {
            $("#video_dont_work textarea[name='add_reason']").hide();
        }
    });

    // Отправка формы видео не работает
    $(".video_dont_work_send_btn").click(function () {
        var modal = $("#video_dont_work");
        var data = modal.find('form').serialize();
        $.ajax({
            url: '/video/dont_work/',
            data: data,
            method: 'post',
            dataType: 'json',
            success: function (response) {
                if(response.status == true) {
                    var html = modal.find('.modal-body').html();
                    modal.find('.modal-body').empty().append('<div class="alert alert-success">Ваше сообщение успешно отправлено.</div>'+html);
                }
            }
        })
    });

    // Открытие формы отправки видео и перенос в нее data параметров
    $('#add_video').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var object_type = button.data('object-type');
        var object_id = button.data('object-id');
        var url = button.data('page-url');
        var modal = $(this);
        modal.find('input[name="object_type"]').val(object_type);
        modal.find('input[name="object_id"]').val(object_id);
        modal.find('input[name="url"]').val(url);
    });

    // Отправляем данные из формы добавить видео
    $(".add_video_btn").click(function () {
        var modal = $("#add_video");
        var data = modal.find('form').serialize();
        $.ajax({
            url: '/video/add_video/',
            data: data,
            method: 'post',
            dataType: 'json',
            beforeSend: function() {
                $("#add_video").find(".has-error").removeClass("has-error");
                $("#add_video").find(".help-block").empty();
            },
            success: function (response) {
                if(response.status == true) {
                    modal.find('.modal-body').empty().append('<div class="alert alert-success">Ваше видео отправлено.</div>');
                    modal.find('.add_video_btn').hide();
                } else {
                    $.each(response.errors, function(key, item){
                        $("#add_video").find('[name='+key+']').parent().addClass('has-error');
                        $("#add_video").find('[name='+key+']').parent().find('.help-block').append(item[0]);
                    });
                }
            }
        })
    });

    $(document).on('click', "input[name=status_showing_results]", function () {
        $.cookie('status_result', $(this).prop('checked'));
    })

});

function rp_filter_fighters() {
        var gender = $(".fighter-rate-block-gender-filter li.active").attr('data-param')
        var action = '/ajax/get_rp_fighter_rate/';
        var block  = '.fighter-rate-list';
        var sport = $('ul.filter-tabs[data-block=".fighter-rate-list"] li.active').attr('data-param');
        var weight_category = $("input[name=rp_rate_weight_category]").val();
        $.ajax({
            url: action,
            method: 'post',
            data: 'param=' + sport + '&gender=' + gender + '&weight_category=' + weight_category,
            dataType: 'json',
            success: function (data) {
                $(block).empty().html(data.html);
            }
        });
}

function rebind_news_mainpage_pagination() {
    $(".news_paginator_on_main li a").bind('click', function () {
        event.preventDefault();
        var action = '/ajax/get_last_news/';
        var block = '.news-list';
        var param = $("ul[data-block='.news-list']").find('li.active').attr('data-param');
        var next_page = $(this).attr('data-page');
        var count_pages = $(this).parent().parent().attr('data-count-pages');
        $.ajax({
            url: action,
            method: 'post',
            data: 'param=' + param + '&page=' + next_page,
            dataType: 'json',
            success: function (data) {
                $(block).empty().html(data.html);
                var p = $(".news_paginator_on_main .page-link.pactive").text();
                $(".news_paginator_on_main .page-link.pactive").removeClass("pactive").empty().append("<a href='#' data-page='"+p+"'>"+p+"</a>");
                $(".news_paginator_on_main .page-link a[data-page="+next_page+"]").parent().empty().append(next_page).addClass('pactive');
                if (next_page != 1) {
                    $(".news_paginator_on_main .prev-link").show().find('a').attr('data-page', parseInt(next_page)-1);
                } else  {
                    $(".news_paginator_on_main .prev-link").hide();
                }
                if (next_page != count_pages) {
                    $(".news_paginator_on_main .next-link").show().find('a').attr('data-page', parseInt(next_page)+1);
                } else  {
                    $(".news_paginator_on_main .next-link").hide();
                }
                rebind_news_mainpage_pagination();
            }
        });
    });
}


function _fixed_menu(screen_width, scroled_top, menu_position) {
    if (screen_width < 768) {
        if (scroled_top >= menu_position && $(".main-menu.hidden-lg.hidden-md.hidden-sm").css('position') != 'fixed') {
            $(".main-menu.hidden-lg.hidden-md.hidden-sm").css({
                position: 'fixed',
                top: 0,
                width: '100%',
                zIndex: 4,
            });
            $(".content").css({
                marginTop: 15 + $(".main-menu.hidden-lg.hidden-md.hidden-sm").height()
            });
        }
        if (scroled_top <= $(".header-row").height() && $(".main-menu.hidden-lg.hidden-md.hidden-sm").css('position') == 'fixed') {
            $(".main-menu.hidden-lg.hidden-md.hidden-sm").css({
                position: 'static',
                top: 0,
                width: '100%',
                zIndex: 4,
            });
            $(".content").css({
                marginTop: parseFloat($(".content").css('marginTop')) - $(".main-menu.hidden-lg.hidden-md.hidden-sm").height()
            });
        }
    }
}

// Функция поправляющая блок с анонсом событий если шильдики с видами спорта не помещаются в одну линию
function _events_block_sport_lables() {
    var blocks = $(".best-event-link-container");
    // Скидываем значения к исходным
    $.each(blocks, function(i, item){
        $(item).find('.best-event-content').css('width', 'auto');
        $(item).find('.best-fights-list-header.best-event-list-header').css('padding-top', '15px');
        $(item).find('.best-event-play-btn').css('top', '20px');
    });
    // Делаем необходимые ресайзы
    $.each(blocks, function(i, item){
       if($(item).outerHeight() > 215) {
           console.log($(item).find('.best-event-content'));
           $(item).find('.best-event-content').css('width', '95px');
           $(item).find('.best-fights-list-header.best-event-list-header').css('padding-top', '8px');
           $(item).find('.best-event-play-btn').css('top', '3px');
       } else {
           $(item).find('.best-event-content').css('width', 'auto');
           $(item).find('.best-fights-list-header.best-event-list-header').css('padding-top', '15px');
           $(item).find('.best-event-play-btn').css('top', '20px');
       }
    });
}