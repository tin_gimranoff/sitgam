from django.db.models import F, Avg
from django.shortcuts import render, redirect, get_object_or_404

from fighting.models import FightStarRate, Fighter, Fight, Event
from settings.models import KindOfFight


def rate(request, sport, type, rate):
    rates_second_lvl = (
        {'alias': 'fights', 'name': 'Бои', 'name_for_title': 'бои'},
        {'alias': 'events', 'name': 'События', 'name_for_title': 'события'},
        {'alias': 'fighters', 'name': 'Бойцы', 'name_for_title': 'бойцы'}
    )
    rates_third_lvl = (
        {'alias': 'human', 'name': 'Зрелищные', 'name_for_title': 'Самые зрелищные'},
        {'alias': 'system', 'name': 'Рейтинговые', 'name_for_title': 'Самые рейтинговые'}
    )
    sports = KindOfFight.objects.order_by('name').all()
    kinda_sport = get_object_or_404(KindOfFight, alias=sport)
    title = ""
    for item in rates_third_lvl:
        if item['alias'] == rate:
            title += item['name_for_title'] + ' '
    for item in rates_second_lvl:
        if item['alias'] == type:
            title += item['name_for_title'] + ' — ' + kinda_sport.name

    data = {
        'sports': sports,
        'rates_second_lvl': rates_second_lvl,
        'rates_third_lvl': rates_third_lvl,
        'title': title,
        'sport': kinda_sport
    }
    if type == 'fighters':
        if rate == 'human':
            fighters = Fighter.objects.all()
            fighters = sorted(fighters, key=lambda t: t.get_human_rate(kind_of_fight_name=kinda_sport.name)['point'], reverse=True)[:100]
        if rate == 'system':
            fighters_rate = FightStarRate.objects.filter(kind_of_fight=kinda_sport).order_by('-points')[:100]
            fighters = [f.fighter for f in fighters_rate]
        data['fighters'] = enumerate(fighters)
        return render(request, 'fighters.html', data)

    if type == 'fights':
        if rate == 'human':
            fights = Fight.objects.filter(kind_of_fight=kinda_sport).annotate(Avg('fighthumanrate__point')).order_by(F('fighthumanrate__point__avg').desc(nulls_last=True))[:100]
        if rate == 'system':
            fights = Fight.objects.filter(kind_of_fight=kinda_sport).all()
            fights = sorted(fights, key=lambda t: t.get_star_rate(), reverse=True)[:100]
        data['fights'] = fights
        return render(request, 'fights.html', data)

    if type == 'events':
        events = Event.objects.all()
        if rate == 'human':
            events = sorted(events, key=lambda t: t.get_human_rate(kind_of_fight_name=kinda_sport.name)['point'], reverse=True)[:100]
        if rate == 'system':
            events = sorted(events, key=lambda t: t.get_star_rate(kind_of_fight_name=kinda_sport.name), reverse=True)[:100]
        data['events'] = events
        return render(request, 'events.html', data)


def detect_first_rate_and_redirect(request):
    sport = KindOfFight.objects.order_by('name').first()
    return redirect('/rate/%s/fights/human/' % sport.alias)
