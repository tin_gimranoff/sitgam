from django.shortcuts import render


def error_404_view(request, exception):
    response = render(request,'404.html')
    response.status_code = 404
    return response
