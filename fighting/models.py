import random

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q, Avg, Count, Window, F
from django.db.models.functions import RowNumber
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.utils.datetime_safe import datetime
from datetime import timedelta
from transliterate import translit
from easy_thumbnails.fields import ThumbnailerImageField
from settings.models import Country, KindOfFight, KindOfFinish, KindOfWin, Titles, Promotion
from settings.models import WeightCategory
from settings.models import Stand
from settings.models import SlugModel


class Fighter(SlugModel):
    GENDER_CHOISES = (
        ('male', 'Мужской'),
        ('female', 'Женский'),
    )

    def __str__(self):
        return "%s (%s)" % (self.name, self.nickname)

    class Meta:
        db_table = 'fighters'
        verbose_name = u'Боец'
        verbose_name_plural = u'Бойцы'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Полное имя')
    name_eng = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Полное имя на английском', help_text=u'Если пустое, то заполняется из поля "Полное имя"')
    nickname = models.CharField(max_length=255, blank=True, null=True, default=None, verbose_name=u'Прозвище')
    nickname_eng = models.CharField(max_length=255, blank=True, null=True, default=None, verbose_name=u'Прозвище на английском', help_text=u'Если пустое, то заполняется из поля "Прозвище"')
    gender = models.CharField(max_length=10, choices=GENDER_CHOISES, default='male', verbose_name=u'Пол бойца')
    country_of_birth = models.ForeignKey(Country, verbose_name=u'Страна рождения', on_delete=models.SET_NULL, null=True, blank=True, related_name='country_of_birth')
    birthday = models.DateField(verbose_name=u'Дата рождения', null=True, blank=True)
    place_of_birth = models.CharField(max_length=255, verbose_name=u'Место рождения', null=True, blank=True)
    country_of_residence = models.ForeignKey(Country, verbose_name=u'Страна проживания', on_delete=models.SET_NULL, null=True, blank=True, related_name='country_of_residence')
    place_of_residence = models.CharField(max_length=255, verbose_name=u'Место проживания', null=True, blank=True)
    stand = models.ForeignKey(Stand, verbose_name=u'Стойка', on_delete=models.SET_NULL, null=True, blank=True)
    growth = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=u'Рост', null=True, blank=True, help_text=u'Разделитель точка')
    weight = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=u'Вес', null=True, blank=True, help_text=u'Разделитель точка')
    arm_span = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=u'Размах рук', null=True, blank=True, help_text=u'Разделитель точка')
    image_195_300 = ThumbnailerImageField(upload_to='fighter/', blank=True, null=True, verbose_name=u'Изображение (195х300)', resize_source=dict(size=(195, 300), crop=True))
    image_55_55 = ThumbnailerImageField(upload_to='fighter/', blank=True, null=True, verbose_name=u'Изображение (55х55)', resize_source=dict(size=(55, 55), crop=True))
    image_36_36 = ThumbnailerImageField(upload_to='fighter/', blank=True, null=True, verbose_name=u'Изображение (36х36)', resize_source=dict(size=(36, 36), crop=True))
    about = models.TextField(default=None, blank=True, null=True, verbose_name=u'О бойце')

    def save(self, *args, **kwargs):
        if self.name_eng is None:
            self.name_eng = translit(str(self.name), 'ru', reversed=True)
        if self.nickname_eng is None:
            self.nickname_eng = translit(str(self.nickname), 'ru', reversed=True)
        super().save(add_id_to_end_url=True, *args, **kwargs)

    # Возраст бойца
    def age(self):
        try:
            d1 = datetime.now().date()
            d2 = self.birthday
            return int((d1 - d2).days / 365)
        except:
            return u'Не указан'

    # Формируем массив со статистикой
    def get_statistic_data(self):
        # Забиарем все виды спорта
        sports = KindOfFight.objects.order_by('name').all()
        # Забиарем все варианты побед/проигрышей
        kinds_of_finish = KindOfFinish.objects.all()
        data = dict()
        for sport in sports:
            # Ищем бои по коонкретному виду
            fights = Fight.objects.filter(kind_of_fight=sport).filter(Q(fighter_1=self) | Q(fighter_2=self)).all()
            # Если количество этих видов более 0
            if len(fights) > 0:
                # Формируем массив для вида со словарями где ключи являются различные исходы боя
                data[sport.name] = {
                    'wins': {'count': 0, 'details': {a.name: 0 for a in kinds_of_finish}},
                    'loose': {'count': 0, 'details': {a.name: 0 for a in kinds_of_finish}},
                    'tie': 0,
                    'no_info': 0
                }
                data[sport.name]['wins']['details']['Неизвестно'] = 0
                data[sport.name]['loose']['details']['Неизвестно'] = 0
                # Идем по каждому бою и смотрим на исход и записываем в нужное место
                for fight in fights:
                    # Если в победителе стоит исходный боец, победитель не пуст и не ничья
                    if fight.winner is not None and fight.winner == self and not fight.tie:
                        data[sport.name]['wins']['count'] += 1
                        # Если поле финиш заполнено увеличиваем одно из них в нашем массиве
                        if fight.finish is not None:
                            data[sport.name]['wins']['details'][fight.finish.name] += 1
                        else:
                            data[sport.name]['wins']['details']['Неизвестно'] += 1
                    # Если в победителе стоит не исходный боец
                    elif fight.winner is not None and fight.winner != self and not fight.tie:
                        data[sport.name]['loose']['count'] += 1
                        # Если поле финиш заполнено увеличиваем одно из них в нашем массиве
                        if fight.finish is not None:
                            data[sport.name]['loose']['details'][fight.finish.name] += 1
                        else:
                            data[sport.name]['loose']['details']['Неизвестно'] += 1
                    # Если стоит ничь и не важно кто там заполнен
                    elif fight.tie:
                        data[sport.name]['tie'] += 1
                    # Если не подходим нигде, указываем - без результат
                    else:
                        data[sport.name]['no_info'] += 1
        return data

    # Формируем статистику по конкретному виду спорта
    def get_statistic_data_by_sport(self, sport):
        # Ищем бои по коонкретному виду
        fights = Fight.objects.filter(kind_of_fight=sport).filter(Q(fighter_1=self) | Q(fighter_2=self)).all()
        data = {
            'wins': 0,
            'loose': 0,
            'tie': 0,
            'no_info': 0
        }
        for fight in fights:
            # Если в победителе стоит исходный боец, победитель не пуст и не ничья
            if fight.winner is not None and fight.winner == self and not fight.tie:
                data['wins'] += 1
            # Если в победителе стоит не исходный боец
            elif fight.winner is not None and fight.winner != self and not fight.tie:
                data['loose'] += 1
                # Если поле финиш заполнено увеличиваем одно из них в нашем массиве
            # Если стоит ничь и не важно кто там заполнен
            elif fight.tie:
                data['tie'] += 1
            # Если не подходим нигде, указываем - без результат
            else:
                data['no_info'] += 1
        return data

    # Весовые категории согласно последним боям
    def get_weight_categories(self):
        fights = Fight.objects.values('kind_of_fight__name', 'weight_category__name').all().filter(Q(fighter_1=self) | Q(fighter_2=self)).order_by('kind_of_fight', '-event__event_date').distinct(
            'kind_of_fight')
        return fights

    # Титулы. Попробуем идти по всем титулам, смотреть кто крайний победитель, если наш боец то отправлять в словарь
    def get_titles(self):
        titles = Titles.objects.all()
        fighter_titles = []
        for title in titles:
            fight = Fight.objects.filter(Q(fighter_1_titles__in=[title]) | Q(fighter_2_titles__in=[title])).order_by('-event__event_date').first()
            if fight and ((fight.winner == self and title in fight.fighter_1_titles.all() and fight.fighter_1 == self) or (
                    fight.winner == self and title in fight.fighter_2_titles.all() and fight.fighter_2 == self)):
                fighter_titles.append({
                    'id': title.pk,
                    'name': title.name,
                })
        return fighter_titles

    # Получаем звездный рейтинг бойца и если его в БД еще нет то создаем
    def get_star_rate(self, kind_of_fight):
        rate = FightStarRate.objects.filter(fighter=self, kind_of_fight=kind_of_fight).first()
        if not rate:
            rate = FightStarRate(fighter=self, kind_of_fight=kind_of_fight)
            rate.save()
        return rate

    # Получаем звездный рейтинг сразу по всем видам спорта
    def get_star_rate_for_all_sports(self):
        sports = KindOfFight.objects.order_by('name').all()
        result = []
        for s in sports:
            result.append({
                'sport': s.name,
                'rate': self.get_star_rate(s),
            })
        return result

    # Берем для бойцов человеческий рейтинг как средний рейтинг всех боев
    def get_human_rate(self, kind_of_fight_name=None):
        # Ищем все бои с данным бойцом
        fights = Fight.objects.filter(Q(fighter_1=self) | Q(fighter_2=self))
        if kind_of_fight_name is not None:
            fights = fights.filter(kind_of_fight__name=kind_of_fight_name)
        fights = fights.all()
        # Если боев нет, то возвращаем ноль и все
        if len(fights) == 0:
            return {
                'point': 0,
                'count': 0,
            }
        sum_points = 0
        sum_marks = 0
        for fight in fights:
            fight_rate = fight.get_human_rate()
            sum_points += float(fight_rate['point'].replace(',', '.'))
            sum_marks += fight_rate['count']
        avg_points = round(sum_points / len(fights), 2)
        return {
            'point': avg_points,
            'count': sum_marks,
        }


class Event(SlugModel):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'events'
        verbose_name = u'Событие'
        verbose_name_plural = u'События'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название события')
    name_eng = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Название события на английском')
    event_date = models.DateField(verbose_name=u'Дата события', null=False, blank=False)
    country = models.ForeignKey(Country, verbose_name=u'Страна', null=True, blank=True, on_delete=models.SET_NULL)
    place = models.CharField(max_length=255, verbose_name=u'Место проведения', null=True, blank=True)
    promotion = models.ManyToManyField(Promotion, verbose_name=u'Промоутеры', blank=True)
    about = models.TextField(verbose_name=u'О событии', blank=True, null=True)
    broadcast = models.CharField(max_length=255, verbose_name=u'Трансляция', blank=True, null=True)
    video = models.CharField(max_length=255, verbose_name=u'Видео', blank=True, null=True)
    image = ThumbnailerImageField(upload_to='event/', blank=True, null=True, verbose_name=u'Изображение (138х205)',
                                  resize_source=dict(size=(138, 205), crop=True), help_text=u'Масштабирование автоматическое')

    def get_sports_dict(self):
        sport_list = self.fight_set.all().values_list('kind_of_fight__name', 'kind_of_fight__color').distinct()
        return sport_list

    def get_video_link(self):
        link = None
        if self.video:
            link = self.video
            return link
        else:
            fights = self.fight_set.all()
            for fight in fights:
                if fight.main_video:
                    link = fight.main_video
                    return link
        return link

    # Берем для бойцов человеческий рейтинг как средний рейтинг всех боев
    def get_human_rate(self, kind_of_fight_name=None):
        # Ищем все бои с данным бойцом
        fights = Fight.objects.filter(event=self)
        if kind_of_fight_name is not None:
            fights = fights.filter(kind_of_fight__name=kind_of_fight_name)
        fights = fights.all()
        # Если боев нет, то возвращаем ноль и все
        if len(fights) == 0:
            return {
                'point': 0,
                'count': 0,
            }
        sum_points = 0
        sum_marks = 0
        for fight in fights:
            fight_rate = fight.get_human_rate()
            sum_points += float(fight_rate['point'].replace(',', '.'))
            sum_marks += fight_rate['count']
        avg_points = round(sum_points / len(fights), 2)
        return {
            'point': avg_points,
            'count': sum_marks,
        }

    # Считаем звездный рейтинг
    def get_star_rate(self, kind_of_fight_name=None):
        fights = self.fight_set
        if kind_of_fight_name is not None:
            fights = fights.filter(kind_of_fight__name=kind_of_fight_name)
        fights = fights.all()
        fights = sorted(fights, key=lambda t: t.get_star_rate(), reverse=True)
        if len(fights) == 0:
            return 0
        if len(fights) == 1:
            return round(fights[0].get_star_rate(), 1)
        if len(fights) == 2:
            return round((fights[0].get_star_rate() + fights[1].get_star_rate()) / 2, 1)
        if len(fights) > 2:
            sum = 0
            count = 0
            for i, fight in enumerate(fights):
                if i > 1:
                    sum += fight.get_star_rate()
                    count += 1
            return round((fights[0].get_star_rate() + fights[1].get_star_rate() + (sum / count)) / 3, 1)

    # Собираем список событий для правой панели
    @classmethod
    def get_rp_best_events(cls, sport):
        # Старый механизм получения списка ретинга событий
        events = Event.objects.filter(fight__kind_of_fight=sport).distinct()
        events = sorted(events, key=lambda t: t.get_human_rate()['point'], reverse=True)
        # Новый механизм получения списка рейтингов событий
        if len(events) > 10:
            random_position = random.randint(0, len(events) - 10)
            events = events[random_position:random_position + 10]
        return events


class Fight(models.Model):

    def __str__(self):
        return "%s vs %s" % (self.fighter_1.name, self.fighter_2.name)

    class Meta:
        db_table = 'fights'
        verbose_name = u'Бой'
        verbose_name_plural = u'Бои'

    alias = models.CharField(max_length=255, verbose_name=u'URL', null=True, blank=True, default=None, help_text=u'Если оставить пустым заполнится автоматически')
    time = models.TimeField(verbose_name=u'Время боя', null=True, blank=True, help_text=u'Дата берется из события', default='06:00')
    kind_of_fight = models.ForeignKey(KindOfFight, blank=True, null=True, on_delete=models.SET_NULL, verbose_name=u'Вид бового искусства')
    fighter_1 = models.ForeignKey(Fighter, blank=False, null=False, verbose_name=u'Боец 1', related_name='fighter_1', on_delete=models.CASCADE)
    fighter_2 = models.ForeignKey(Fighter, blank=False, null=False, verbose_name=u'Боец 2', related_name='fighter_2', on_delete=models.CASCADE)
    weight_category = models.ForeignKey(WeightCategory, null=True, blank=True, on_delete=models.SET_NULL, verbose_name=u'Весовая категория')
    fighter_1_weight_before_fight = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=u'Вес до боя', null=True, blank=True, help_text=u'Разделитель точка')
    fighter_1_titles = models.ManyToManyField(Titles, verbose_name=u'Титулы', blank=True, related_name='fighter_1_titles')
    fighter_2_weight_before_fight = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=u'Вес до боя', null=True, blank=True, help_text=u'Разделитель точка')
    fighter_2_titles = models.ManyToManyField(Titles, verbose_name=u'Титулы', blank=True, related_name='fighter_2_titles')
    winner = models.ForeignKey(Fighter, null=True, blank=True, verbose_name=u'Победитель', related_name='fighter', on_delete=models.SET_NULL)
    tie = models.BooleanField(default=False, verbose_name=u'Поставьте если итогом стала ничья', help_text=u'Победителя тогда можете не выбирать')
    main_video = models.CharField(max_length=255, null=True, blank=True, verbose_name=u'Основное видео боя',
                                  help_text=u'Пример корректной ссылки: https://www.youtube.com/watch?v=WdodEsDwTIA .Вы можете добавить дополнительные видо в модуле - видео')
    finish = models.ForeignKey(KindOfFinish, blank=True, null=True, verbose_name=u'Вид окончания боя', help_text=u'Необходимо для статистики на странице бойца', on_delete=models.SET_NULL)
    finish_round = models.IntegerField(null=True, blank=True, default=None, verbose_name=u'Раунд окончания боя')
    finish_time = models.CharField(max_length=5, null=True, blank=True, verbose_name=u'Время окончания боя')
    judge = models.CharField(max_length=255, verbose_name=u'Судья', blank=True, null=True)
    kind_of_finish = models.ForeignKey(KindOfWin, verbose_name=u'Способ поеды', blank=True, null=True, on_delete=models.SET_NULL)
    comment = models.TextField(verbose_name=u'Дополнительно', null=True, blank=True)
    about = models.TextField(verbose_name=u'О бое', null=True, blank=True)
    event = models.ForeignKey(Event, verbose_name=u'Событие', null=False, blank=False, on_delete=models.PROTECT)
    score_fighter_1 = models.IntegerField(verbose_name=u'Очки бойца 1 до боя', null=True, blank=True)
    score_fighter_2 = models.IntegerField(verbose_name=u'Очки бойца 2 до боя', null=True, blank=True)

    @property
    def fighter_1_age(self):
        try:
            return self.detect_fighter_age(self.fighter_1)
        except:
            return u'Не указан'

    @property
    def fighter_2_age(self):
        try:
            return self.detect_fighter_age(self.fighter_2)
        except:
            return u'Не указан'

    @property
    def fighter_1_statistic(self):
        return self.detect_fighter_statistic(self.fighter_1)

    @property
    def fighter_2_statistic(self):
        return self.detect_fighter_statistic(self.fighter_2)

    def finish_str(self):
        str = ''
        str += self.finish.name if self.finish else ''
        str += ' (%s)' % self.kind_of_finish.name if self.finish and self.kind_of_finish else ''
        return str

    def get_previous_fight(self):
        return Fight.objects. \
            filter(Q(event__event_date__lt=self.event.event_date) | Q(event__event_date=self.event.event_date, time__lt=self.time)). \
            filter(
            Q(fighter_1=self.fighter_1) | Q(fighter_2=self.fighter_1) | Q(fighter_1=self.fighter_2) | Q(fighter_2=self.fighter_2)
        ).filter(kind_of_fight=self.kind_of_fight).filter(Q(winner__isnull=False) | Q(tie=True)). \
            exclude(pk=self.pk). \
            order_by('-event__event_date', '-time'). \
            first()

    def get_next_fight(self):
        return Fight.objects. \
            filter(Q(event__event_date__gt=self.event.event_date) | Q(event__event_date=self.event.event_date, time__gt=self.time)). \
            filter(
            Q(fighter_1=self.fighter_1) | Q(fighter_2=self.fighter_1) | Q(fighter_1=self.fighter_2) | Q(fighter_2=self.fighter_2)
        ).filter(kind_of_fight=self.kind_of_fight).filter(Q(winner__isnull=False) | Q(tie=True)).exclude(pk=self.pk). \
            order_by('event__event_date', 'time'). \
            first()

    def clean(self):
        super().clean()
        if self.time is None:
            self.time = datetime.strptime("06:00:00", "%H:%M:%S")
        # Делаем так как будтно у нас уже есть в это время бой
        exist_fight_in_event_same_time = -1
        # Пока у нас этот флаг пока зывает что у нас есть бои будем прибавлять по 20 минут
        while exist_fight_in_event_same_time != 0:
            # Будем валидировать дату в событии у нас должен быть только один бой
            exist_fight_in_event_same_time = Fight.objects.filter(event=self.event, time=self.time)
            if self.pk is None:
                exist_fight_in_event_same_time = exist_fight_in_event_same_time.count()
            else:
                exist_fight_in_event_same_time = exist_fight_in_event_same_time.exclude(pk=self.pk).count()
            if exist_fight_in_event_same_time != 0:
                self.time = (datetime.combine(datetime.now(), self.time) + timedelta(minutes=20)).time()

    def save(self, *args, **kwargs):
        # Флаг о необходимости пересчета просто объявляем тут
        recalculate_after_save_from_previous = False
        # Если у нас объект новый то все отлично все легко
        if self.pk is None:
            # Заполняем рейтинг по бою по умолчанию, то есть тот который был до боя, на случай отката
            # По бойцу 1
            try:
                self.score_fighter_1 = FightStarRate.objects.get(kind_of_fight=self.kind_of_fight, fighter=self.fighter_1).points
            except ObjectDoesNotExist:
                self.score_fighter_1 = None
            # По бойцу 2
            try:
                self.score_fighter_2 = FightStarRate.objects.get(kind_of_fight=self.kind_of_fight, fighter=self.fighter_2).points
            except ObjectDoesNotExist:
                self.score_fighter_2 = None
            # Если у нас указан победитель или ничья то считаем что результат есть и мы можем учитывать этот бой
            if self.winner is not None or self.tie == True:
                # Ищем есть ли следующий бой
                next_fight = self.get_next_fight()
                # Ищем есть ли предыдущий бой
                previous_fight = self.get_previous_fight()
                # Если потом боев нет не надо ничего пересчитывать просто считаем все и все
                if next_fight is None:
                    self.calculate_rate()
                else:
                    # Если предыдущего боя нет, то этот бой первый будет. Выставляем все рейтинги в None
                    if previous_fight is None:
                        self.score_fighter_1 = 1
                        self.score_fighter_2 = 1
                    # Ставим флаг что после сохранения пересчитываем все бои начиная с предыдущего. Ну или с этого если этот бой первый
                    recalculate_after_save_from_previous = True
        else:
            # Пока объект не сохранили берем его текущие параметры по PK
            object = Fight.objects.get(pk=self.pk)
            # Если бой не новый но у него не было результата то пересчитывать нам ничего не надо
            # Просто запускаем рассчет баллов
            if (object.winner is None and object.tie == False) and (self.winner is not None or self.tie == True):
                self.calculate_rate()
            # Если у нас изменилися победитель
            # Или если изменилась галочка ничья
            # Или изменилась сопособ победы
            # Или изменился вид окончания боя
            elif object.winner != self.winner or object.tie != self.tie or object.kind_of_finish != self.kind_of_finish or object.finish != self.finish:
                # Запускаем пересчет рейтинга от этого боя
                self.recalculate_rate()

        if self.alias is not None and self.alias != '':
            alias_translit = slugify(translit(str(self.alias), 'ru', reversed=True))
            if self.alias != alias_translit:
                self.alias = alias_translit
        super().save(*args, **kwargs)
        # Формируем URL
        if self.alias is None or self.alias == '':
            try:
                if self.fighter_1.name_eng is not None:
                    fighter_1_url = slugify(translit(str(self.fighter_1.name_eng), 'ru', reversed=True))
                else:
                    fighter_1_url = slugify(translit(str(self.fighter_1.name), 'ru', reversed=True))
            except:
                fighter_1_url = translit(self.fighter_1.name, 'ru', reversed=True),

            try:
                if self.fighter_2.name_eng is not None:
                    fighter_2_url = slugify(translit(str(self.fighter_2.name_eng), 'ru', reversed=True))
                else:
                    fighter_2_url = slugify(translit(str(self.fighter_2.name), 'ru', reversed=True))
            except:
                fighter_2_url = translit(self.fighter_2.name, 'ru', reversed=True),
            self.alias = slugify("%s_%s_%s" % (
                fighter_1_url,
                fighter_2_url,
                self.id
            ))
            self.save()

        # Если существует флаг и он TRUE
        if recalculate_after_save_from_previous:
            # Если есть предыдущий бой
            if previous_fight is not None:
                # Персчитываем все с предыдущего боя
                self.recalculate_rate(start_recalculate_object=previous_fight)
            else:
                # Если нет. То считаем что этот бой первый и пересчитываем с него.
                self.recalculate_rate()

    def calculate_rate(self, recalculate=False, start_item=None):
        # Получаем рейтинг текущего бойца 1 если их нет то создаем эти объекты
        try:
            # Ищем рейтинг для бойка в БД
            points_1_obj = FightStarRate.objects.get(kind_of_fight=self.kind_of_fight, fighter=self.fighter_1)
            # Смотрим если у нас не указано что мы что-то пересчитываем или боец не равен одному из тех кто был изначально
            if start_item is None or (self.fighter_1 == start_item.fighter_1 or self.fighter_1 == start_item.fighter_2):
                # Берем рейтинг который мы посчитали уже в БД в прошлом бое
                points_1 = points_1_obj.points
            else:
                # Если же у нас новый боец в паре то берем рейтинг тот который у него был до боя с этим бойцом
                points_1 = 1 if self.score_fighter_1 is None else self.score_fighter_1
        except ObjectDoesNotExist:
            # Если рейтинга в БД и не было скорее всего это первый бой у бойцом выставляем им все в 1
            points_1_obj = FightStarRate(kind_of_fight=self.kind_of_fight, fighter=self.fighter_1)
            points_1 = 1
        # Получаем рейтинг текущего бойца 2 если их нет то создаем эти объекты
        try:
            points_2_obj = FightStarRate.objects.get(kind_of_fight=self.kind_of_fight, fighter=self.fighter_2)
            # Смотрим если у нас не указано что мы что-то пересчитываем или боец не равен одному из тех кто был изначально
            if start_item is None or (self.fighter_2 == start_item.fighter_1 or self.fighter_2 == start_item.fighter_2):
                # Берем рейтинг который мы посчитали уже в БД в прошлом бое
                points_2 = points_2_obj.points
            else:
                # Если же у нас новый боец в паре то берем рейтинг тот который у него был до боя с этим бойцом
                points_2 = 1 if self.score_fighter_2 is None else self.score_fighter_2
        except ObjectDoesNotExist:
            # Если рейтинга в БД и не было скорее всего это первый бой у бойцом выставляем им все в 1
            points_2_obj = FightStarRate(kind_of_fight=self.kind_of_fight, fighter=self.fighter_2)
            points_2 = 1

        if recalculate:
            self.score_fighter_1 = points_1
            self.score_fighter_2 = points_2

        # Points 1 всегда очки победителя
        # Так что для рассчета если надо их меняем местами
        # Так что если победил не первый боец, то
        if self.winner == self.fighter_2:
            points_tmp = points_1
            points_1 = points_2
            points_2 = points_tmp

        all_points = 25 + points_2 + points_1
        m = 0.7 + 0.6 * (points_1 / (points_1 + points_2))
        if self.finish is not None and self.finish.pk in {1, 3, 4}:
            n = 0.75
        elif (self.finish is not None and self.kind_of_finish is not None and self.finish.pk == 2 and self.kind_of_finish.pk == 2) or (self.finish is not None and self.finish.pk == 5):
            n = 0.65
        elif (self.finish is not None and self.kind_of_finish is not None and self.finish.pk == 2 and self.kind_of_finish.pk == 3):
            n = 0.55
        elif (self.finish is not None and self.kind_of_finish is not None and self.finish.pk == 2 and self.kind_of_finish.pk == 4):
            n = 0.53
        elif self.tie:
            n = 0.5
        elif self.finish is not None and self.finish.pk == 7:
            n = 0.6

        points_winner = round(m * n * all_points)
        points_looser = all_points - points_winner

        if self.winner == self.fighter_1 or self.tie:
            points_1_obj.points = points_winner
            points_2_obj.points = points_looser
        elif self.winner == self.fighter_2:
            points_1_obj.points = points_looser
            points_2_obj.points = points_winner

        points_1_obj.save()
        points_2_obj.save()

        return {'points_1': points_winner, 'points_2': points_looser}

    def recalculate_rate(self, start_recalculate_object=False, exclude_object=None):
        # Если объект с которого надо пересчитывать не задан
        # считаем что это текущий обект
        if not start_recalculate_object:
            _start_recalculate_object = self
        else:
            _start_recalculate_object = start_recalculate_object
        # Получаем рейтинг текущего бойца 1 если их нет то создаем эти объекты
        try:
            points_1_obj = FightStarRate.objects.get(kind_of_fight=_start_recalculate_object.kind_of_fight, fighter=_start_recalculate_object.fighter_1)
        except ObjectDoesNotExist:
            points_1_obj = FightStarRate(kind_of_fight=_start_recalculate_object.kind_of_fight, fighter=_start_recalculate_object.fighter_1)
        points_1_obj.points = 1 if _start_recalculate_object.score_fighter_1 is None else _start_recalculate_object.score_fighter_1
        points_1_obj.save()
        # Получаем рейтинг текущего бойца 2 если их нет то создаем эти объекты
        try:
            points_2_obj = FightStarRate.objects.get(kind_of_fight=_start_recalculate_object.kind_of_fight, fighter=_start_recalculate_object.fighter_2)
        except ObjectDoesNotExist:
            points_2_obj = FightStarRate(kind_of_fight=_start_recalculate_object.kind_of_fight, fighter=_start_recalculate_object.fighter_2)
        points_2_obj.points = 1 if _start_recalculate_object.score_fighter_2 is None else _start_recalculate_object.score_fighter_2
        points_2_obj.save()

        _start_recalculate_object.calculate_rate()
        # Мы выстраиваем бои так что мы берем все следующие бои просто по дате, либо бои с такой же датой, но тогда по времени после
        fights = Fight.objects. \
            filter(Q(event__event_date=_start_recalculate_object.event.event_date, time__gt=_start_recalculate_object.time) | Q(event__event_date__gt=_start_recalculate_object.event.event_date)). \
            filter(
            Q(fighter_1=_start_recalculate_object.fighter_1) | Q(fighter_2=_start_recalculate_object.fighter_1) | Q(fighter_1=_start_recalculate_object.fighter_2) | Q(
                fighter_2=_start_recalculate_object.fighter_2)
        ). \
            filter(kind_of_fight=_start_recalculate_object.kind_of_fight).filter(Q(winner__isnull=False) | Q(tie=True)).exclude(pk=_start_recalculate_object.pk)
        if exclude_object is not None:
            fights = fights.exclude(pk=exclude_object.pk)
        fights = fights.order_by('event__event_date', 'time').all()
        for f in fights:
            f.calculate_rate(recalculate=True, start_item=_start_recalculate_object)
            f.save()

    def detect_fighter_age(self, fighter):
        d1 = self.event.event_date
        d2 = fighter.birthday
        return int((d1 - d2).days / 365)

    def detect_fighter_statistic(self, fighter):
        fights = Fight.objects.filter(Q(fighter_1=fighter) | Q(fighter_2=fighter)).filter(event__event_date__lt=self.event.event_date).all()
        wins = 0
        loose = 0
        tie = 0
        for fight in fights:
            if fight.tie:
                tie += 1
            elif fight.winner == fighter:
                wins += 1
            elif fight.winner is not None and fight.winner != fighter:
                loose += 1
        return {'wins': wins, 'loose': loose, 'tie': tie}

    def get_titles_for_fight(self):
        fighter_1_titles = self.fighter_1_titles.all()
        fighter_2_titles = self.fighter_2_titles.all()
        titles = {}
        for title in fighter_1_titles:
            titles[title.pk] = {'link': title.alias, 'name': title.name, 'for': self.fighter_1.name}
        for title in fighter_2_titles:
            if title.pk in titles:
                titles[title.pk]['for'] = None
            else:
                titles[title.pk] = {'link': title.alias, 'name': title.name, 'for': self.fighter_2.name}
        return titles.items()

    def get_star_rate(self):
        # Берем рейтинги бойцов
        rates = FightStarRate.objects.filter(Q(fighter=self.fighter_1) | Q(fighter=self.fighter_2)).filter(kind_of_fight=self.kind_of_fight)
        # У нас должно быть 2 объекта
        try:
            rate_1 = rates[0].star_score
            if rate_1 is None:
                rate_1 = 1
        except:
            rate_1 = 1

        try:
            rate_2 = rates[1].star_score
            if rate_2 is None:
                rate_2 = 1
        except:
            rate_2 = 1

        return round((rate_1 + rate_2) / 2, 1)

    def get_human_rate(self):
        rate = FightHumanRate.objects.filter(fight=self).aggregate(Avg('point_in_percents'), Avg('point'), Count('id'))
        return {
            'point_in_percents': str(rate['point_in_percents__avg']).replace(',', '.') if rate['point_in_percents__avg'] is not None else '0',
            'point': str(round(rate['point__avg'], 2)).replace('.', ',') if rate['point__avg'] is not None else '0',
            'count': rate['id__count'],
        }

    # Собираем список боев для правой колонки рейтинга
    @classmethod
    def get_rp_best_fights(cls, sport):
        # Старая весия запроса с сортировкой лучших и выборкой 10 первых
        fights = Fight.objects.filter(kind_of_fight=sport). \
                     annotate(Avg('fighthumanrate__point'), row_number=Window(
            expression=RowNumber(),
            order_by=F('fighthumanrate__point__avg').desc(nulls_last=True)
        )). \
                     order_by(F('fighthumanrate__point__avg').desc(nulls_last=True))[:500]

        # Новая версия получения объектов в с сортировкой как в сгенерированном листе
        print(len(fights))
        if len(fights) > 10:
            random_position = random.randint(0, len(fights) - 10)
            fights = fights[random_position:random_position + 10]
        return fights

    # Собираем список ожидаемых боев для правой колонки
    @classmethod
    def get_rp_wating_fights(cls, sport):
        # Старый механизм основанный на построении от рейтинга
        fights = Fight.objects. \
            filter(Q(event__event_date__gt=datetime.now().date()) | Q(event__event_date=datetime.now().date(), time__gte=datetime.now().time())). \
            filter(kind_of_fight=sport). \
            order_by('event__event_date', 'time'). \
            all()
        waited_fights = []
        for f in fights:
            if f.get_star_rate() >= 3:
                waited_fights.append(f)
        return waited_fights


@receiver(pre_delete, sender=Fight)
def _fight_delete(sender, instance, **kwargs):
    if instance.score_fighter_1 is None and instance.score_fighter_2 is None:
        return True
    print("===========> Началось удаление")
    # Ищем предыдущий бой перед удаляемым
    prev_fight = instance.get_previous_fight()
    print("===========> Предыдущий бой")
    print(prev_fight)
    # Ищем следующий бой после удаляемого
    next_fight = instance.get_next_fight()
    print("===========> Следующий бой")
    print(next_fight)
    # Если у нас предыдущй бой есть
    if prev_fight is not None:
        print("===========> У нас есть предыдущий бой")
        # Проверяем если у нас следующий бой, мало ли мы удаляем последний бой в очереди
        if next_fight is None:
            # У нас нет следующего мы скидываем рейтинг в рейтинг до удаляемого боя
            print("===========> Но нет следующего")
            # Скидываем рейтинг
            FightStarRate.objects.filter(fighter=instance.fighter_1, kind_of_fight=instance.kind_of_fight).update(points=instance.score_fighter_1)
            FightStarRate.objects.filter(fighter=instance.fighter_2, kind_of_fight=instance.kind_of_fight).update(points=instance.score_fighter_2)
        print("===========> Запускаем пересчет начиная с предыдущего и исключая текущий")
        # Запускаем рассчет с предыдущего боя и с пропуском того который надо удалить
        instance.recalculate_rate(start_recalculate_object=prev_fight, exclude_object=instance)
    # Если у нас предыдущего боя нет, то есть бой у нас первый в бое или вообще
    else:
        print("===========> У нас нет прыдыдущего боя скидываем рейтинг до 1")
        # Если у нас нет предыдущих боев связанных в этом спорте с этими бойцами значит у них рейтинг был начальный = 1
        FightStarRate.objects.filter(fighter=instance.fighter_1, kind_of_fight=instance.kind_of_fight).update(points=1)
        FightStarRate.objects.filter(fighter=instance.fighter_2, kind_of_fight=instance.kind_of_fight).update(points=1)
        # Смотрим есть ли следующий бой
        if next_fight is not None:
            # Если в следующем бое есть первый боец, то рейтинг этого бойца к тому бою тоже 1
            # Так как это становится первый бой для этого бойца в этом виде
            if next_fight.fighter_1 == instance.fighter_1 or next_fight.fighter_1 == instance.fighter_2:
                print("===========> Первый боец в следующем бое совпал скидываем рейтинг до 1")
                next_fight.score_fighter_1 = 1
                next_fight.save()
            # Так же самая история про второго бойца
            if next_fight.fighter_2 == instance.fighter_1 or next_fight.fighter_2 == instance.fighter_2:
                print("===========> Второй боец в следующем бое совпал скидываем рейтинг до 1")
                next_fight.score_fighter_2 = 1
                next_fight.save()
            print("===========> Но есть следующий запускаем пересчет со следующего")
            # Зпускаем пересчет со следующего боя
            instance.recalculate_rate(start_recalculate_object=next_fight)
    print("=================> DELETE")


class FightStarRate(models.Model):
    class Meta:
        db_table = 'fight_star_rates'

    kind_of_fight = models.ForeignKey(KindOfFight, null=False, blank=False, on_delete=models.CASCADE)
    fighter = models.ForeignKey(Fighter, null=False, blank=False, on_delete=models.CASCADE)
    points = models.IntegerField(default=1)
    star_score = models.FloatField(default=None, null=True, blank=True)
    position = models.IntegerField(default=None, null=True, blank=True)

    @classmethod
    def get_rp_fighters_rate(cls, sport, gender=None):
        # Старый механизм сортировки бойцов для правой панели
        # fighter_rate = FightStarRate.objects.filter(kind_of_fight=sport).order_by('-points')[:10]
        # Новый механизм сортировки бойцов для отбора для правой панели
        fighters_rates = cls.objects.annotate(row_number=Window(
            expression=RowNumber(),
            order_by=F('star_score').desc()
        )).filter(kind_of_fight=sport, position__isnull=False).order_by('-star_score')[:10]
        if gender is not None:
            fighters_rates = fighters_rates.filter(fighter__gender=gender)
        return fighters_rates


class FightHumanRate(models.Model):
    class Meta:
        db_table = 'fight_human_rate'

    ip_address = models.GenericIPAddressField(null=False, blank=False)
    point_in_percents = models.FloatField(null=False, blank=False, default=0)
    point = models.FloatField(null=False, blank=False, default=0)
    fight = models.ForeignKey(Fight, blank=False, null=False, on_delete=models.CASCADE)
