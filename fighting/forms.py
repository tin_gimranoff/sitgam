from django import forms
from django.db.models import Q, Value
from django.db.models.functions import Concat
from django.utils.datetime_safe import datetime

from fighting.models import Fighter
from settings.models import Promotion


class FightsFilterForm(forms.Form):

    GENDER_CHOISES = (
        ('', 'Любой'),
        ('male', 'Мужской'),
        ('female', 'Женский'),
    )

    EVENT_DATE_CHOISES = (
        ('all', 'Все'),
        ('before', 'Прошедшие'),
        ('after', 'Предстоящие'),
    )

    SORT_CHANGE = (
        ('date_up', 'Дате (по возрастанию)'),
        ('date_down', 'Дате (по убыванию)'),
        ('rate_down', 'Рейтингу (по убыванию)'),
    )

    fighter_1 = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
            'placeholder': u'Имя бойца 1'
        }),
        required=False,
    )

    fighter_2_full = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
            'placeholder': u'Имя бойца 2'
        }),
        required=False,
    )

    fighter_2 = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control hidden-lg hidden-md sm-mt-10',
            'autocomplete': 'off',
            'placeholder': u'Имя бойца 2'
        }),
        required=False
    )

    gender = forms.ChoiceField(choices=GENDER_CHOISES, widget=forms.RadioSelect, required=False)
    gender_2 = forms.ChoiceField(choices=GENDER_CHOISES, widget=forms.RadioSelect, required=False)

    promotion = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
            'placeholder': u'Название промоутера'
        }),
        required=False
    )

    event = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
            'placeholder': u'Название события'
        }),
        required=False
    )

    event_date_from = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control input-date',
            'placeholder': 'От __ __ ____',
            'autocomplete': 'off',
        }),
        required=False
    )

    event_date_to = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control input-date',
            'placeholder': 'До __ __ ____',
            'autocomplete': 'off',
        }),
        required=False
    )

    event_date = forms.ChoiceField(choices=EVENT_DATE_CHOISES, widget=forms.RadioSelect, required=False)

    sort = forms.ChoiceField(choices=SORT_CHANGE, widget=forms.RadioSelect, required=False)

    show_results = forms.BooleanField(widget=forms.CheckboxInput, required=False)

    with_video = forms.BooleanField(widget=forms.CheckboxInput, required=False)

    def apply_filter_to_query_set(self, qs):
        qs_with_filter = qs
        # CHECK FOR VALID DATA IN FORM
        if not self.is_valid():
            return qs
        else:
            # GETTING CLEANED DATA
            data = self.cleaned_data

        # FILTER FOR FIGHTER 1
        if data['fighter_1'] != '':
            fighter = Fighter.objects.annotate(fighter_1_display_name=Concat("name", Value(" ("), "nickname", Value(")"))).get(fighter_1_display_name=data['fighter_1'])
            if fighter is not None:
                qs_with_filter = qs.filter(Q(fighter_1=fighter) | Q(fighter_2=fighter))
            fighter = None

        # FILTER FOR FIGHTING 2 FROM BIG DISPLAY FROM
        if data['fighter_2_full'] != '':
            fighter = Fighter.objects.annotate(fighter_1_display_name=Concat("name", Value(" ("), "nickname", Value(")"))).get(fighter_1_display_name=data['fighter_2_full'])
            if fighter is not None:
                qs_with_filter = qs_with_filter.filter(Q(fighter_1=fighter) | Q(fighter_2=fighter))
            fighter = None

        # FILTER FOR FIGHTING 2 FROM SMALL DISPLAY FROM
        if data['fighter_2'] != '':
            fighter = Fighter.objects.annotate(fighter_1_display_name=Concat("name", Value(" ("), "nickname", Value(")"))).get(fighter_1_display_name=data['fighter_2'])
            if fighter is not None:
                qs_with_filter = qs_with_filter.filter(Q(fighter_1=fighter) | Q(fighter_2=fighter))
            fighter = None

        # FILTER GENDER OF FIGHTERS
        if data['gender'] != '':
            qs_with_filter = qs_with_filter.filter(fighter_1__gender=data['gender'], fighter_2__gender=data['gender'])

        # FILTER PROMOTION
        if data['promotion'] != '':
            qs_with_filter = qs_with_filter.filter(event__promotion__name=data['promotion'])

        # FILTER EVENT NAME
        if data['event'] != '':
            qs_with_filter = qs_with_filter.filter(event__name__icontains=data['event'])

        # FILTE DATE IF USER CHEKCED BEFORE OR AFTER
        if data['event_date'] != '' and data['event_date'] != 'all':
            if data['event_date'] == 'before':
                qs_with_filter = qs_with_filter.filter(event__event_date__lte=datetime.now())
            if data['event_date'] == 'after':
                qs_with_filter = qs_with_filter.filter(event__event_date__gte=datetime.now())

        # FILTER IF USER WHANTS TO SEE FIGHT FROM CONCRETE DATE
        if data['event_date_from']:
            start_date = datetime.strptime(data['event_date_from'], '%d.%m.%Y')
            qs_with_filter = qs_with_filter.filter(event__event_date__gte=start_date)

        # FILTER IF USER WHANTS TO SEE FIGHTS TO CONCRETE DATE
        if data['event_date_to']:
            to_date = datetime.strptime(data['event_date_to'], '%d.%m.%Y')
            qs_with_filter = qs_with_filter.filter(event__event_date__lte=to_date)

        # FILTER IF FIGHT HAS VIDEO
        if data['with_video']:
            qs_with_filter = qs_with_filter.filter(main_video__isnull=False).exclude(main_video='')

        # SORTING THE RESULT
        if data['sort'] != '':
            if data['sort'] == 'date_up':
                qs_with_filter = qs_with_filter.order_by('event__event_date')
            if data['sort'] == 'date_down':
                qs_with_filter = qs_with_filter.order_by('-event__event_date')
        else:
            qs_with_filter = qs_with_filter.order_by('event__event_date')

        # RETURN RESULT
        return qs_with_filter

    def apply_filter_to_event_query_set(self, qs):
        qs_with_filter = qs
        # CHECK FOR VALID DATA IN FORM
        if not self.is_valid():
            return qs
        else:
            # GETTING CLEANED DATA
            data = self.cleaned_data

        # FILTER PROMOTION
        if data['promotion'] != '':
            qs_with_filter = qs_with_filter.filter(promotion__name=data['promotion'])

        # FILTER DATE IF USER CHEKCED BEFORE OR AFTER
        if data['event_date'] != '' and data['event_date'] != 'all':
            if data['event_date'] == 'before':
                qs_with_filter = qs_with_filter.filter(event_date__lte=datetime.now())
            if data['event_date'] == 'after':
                qs_with_filter = qs_with_filter.filter(event_date__gte=datetime.now())

        # FILTER IF USER WHANTS TO SEE EVENTS FROM CONCRETE DATE
        if data['event_date_from']:
            start_date = datetime.strptime(data['event_date_from'], '%d.%m.%Y')
            qs_with_filter = qs_with_filter.filter(event_date__gte=start_date)

        # FILTER IF USER WHANTS TO SEE EVENTS TO CONCRETE DATE
        if data['event_date_to']:
            to_date = datetime.strptime(data['event_date_to'], '%d.%m.%Y')
            qs_with_filter = qs_with_filter.filter(event_date__lte=to_date)

        # SORTING THE RESULT
        if data['sort'] != '':
            if data['sort'] == 'date_up':
                qs_with_filter = qs_with_filter.order_by('event_date')
            if data['sort'] == 'date_down':
                qs_with_filter = qs_with_filter.order_by('-event_date')
        else:
            qs_with_filter = qs_with_filter.order_by('event_date')

        # RETURN RESULT
        return qs_with_filter
