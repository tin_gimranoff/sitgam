from django import forms
from django.contrib import admin
from django.forms import widgets
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from easy_select2 import select2_modelform

from fighting.models import *


class VersionedMediaJS():
    def __init__(self, path, version):
        self.path = forms.widgets.Media.absolute_path(None, path)
        self.version = version

    def render(self):
        html = '<script type="text/javascript" src="{0}?v={1}"></script>'
        return format_html(html, mark_safe(self.path), self.version)

    @staticmethod
    def render_js(media_object):
        html = []
        for path in media_object._js:
            if hasattr(path, 'version'):
                html.append(path.render())
            else:
                html.append(format_html('<script type="text/javascript" src="{0}"></script>', media_object.absolute_path(path)))
        return html


widgets.Media.render_js = VersionedMediaJS.render_js

FightForm = select2_modelform(Fight, attrs={'width': '150px'})


class FightAdmin(admin.ModelAdmin):
    form = FightForm

    class Media:
        js = (
            VersionedMediaJS('js/custom_admin.js', '0.5'),
            VersionedMediaJS('js/jquery.grp_timepicker.js', '0.1'),
        )
        css = {
            'all': ('css/admin.css',)
        }

    change_form_template = 'admin/fight_change_form.html'

    list_display = ('__str__', 'kind_of_fight', 'alias', 'event', 'get_event_date', 'time')

    fieldsets = (
        ('Общая информация', {
            'fields': ('alias', 'kind_of_fight', 'weight_category', 'main_video', 'about', 'event', 'time'),
        }),
        ('Боец 1', {
            'fields': ('fighter_1', 'fighter_1_weight_before_fight', 'fighter_1_titles'),
        }),
        ('Боец 2', {
            'fields': ('fighter_2', 'fighter_2_weight_before_fight', 'fighter_2_titles'),
        }),
        ('Итоги', {
            'fields': ('judge', 'winner', 'tie', 'finish', 'kind_of_finish', 'finish_round', 'finish_time', 'comment'),
        })
    )

    def get_event_date(self, obj):
        return obj.event.event_date

    get_event_date.short_description = 'Дата события'
    get_event_date.admin_order_field = 'event__event_date'


class FighterAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias',)
    change_form_template = 'admin/fighter_change_form.html'


EventForm = select2_modelform(Event, attrs={'width': '200px'})


class EventAdmin(admin.ModelAdmin):
    form = EventForm

    class Media:
        css = {
            'all': ('css/admin.css',)
        }

    change_form_template = 'admin/event_change_form.html'


admin.site.register(Fighter, FighterAdmin)
admin.site.register(Fight, FightAdmin)
admin.site.register(Event, EventAdmin)
