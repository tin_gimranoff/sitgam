from datetime import datetime
from itertools import chain

from django.db.models.functions import Lower, Concat
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.db.models import Q, Value, Avg, Count
from django.template.defaultfilters import lower
from django.views.decorators.csrf import csrf_exempt

from fighting.forms import FightsFilterForm
from fighting.models import Fighter, Fight, Event, FightHumanRate
from settings.models import KindOfFight, Promotion
from sitgam.private_settings import SESSION_DOMAIN
from video.models import Video


def get_fighters_by_name(request):
    if request.GET.get('q'):
        search_fields = ('showing_name',)
        q = request.GET['q']
        data = Fighter.objects.annotate(name_lower=Lower('name'), nickname_lower=Lower('nickname'), name_eng_lower=Lower('name_eng'), nickname_eng_lower=Lower('nickname_eng'), showing_name=Concat('name', Value(' ('), 'nickname', Value(')'))).\
            filter(Q(name_lower__startswith=lower(q)) | Q(nickname_lower__startswith=lower(q)) | Q(name_eng_lower__startswith=lower(q)) | Q(nickname_eng_lower__startswith=lower(q))).\
            values('showing_name')
        json = list()
        for item in data:
            for field in search_fields:
                if item[field] is not None:
                    json.append(item[field])
        return JsonResponse(json, safe=False)
    else:
        HttpResponse("No cookies")


def get_events_by_name(request):
    if request.GET.get('q'):
        q = request.GET['q']
        data = Event.objects.annotate(name_lower=Lower('name')).filter(name_lower__startswith=lower(q)).values_list('name', flat=True)
        json = list(data)
        return JsonResponse(json, safe=False)
    else:
        HttpResponse("No cookies")


def get_promotions_by_name(request):
    if request.GET.get('q'):
        q = request.GET['q']
        data = Promotion.objects.annotate(name_lower=Lower('name')).filter(name_lower__startswith=lower(q)).values_list('name', flat=True)
        json = list(data)
        return JsonResponse(json, safe=False)
    else:
        HttpResponse("No cookies")


def fighter_details(request, alias):
    # Ищем бойца
    fighter = get_object_or_404(Fighter, alias=alias)
    # Подцепляем видео
    videos = Video.objects.filter(Q(fighter_1=fighter) | Q(fighter_2=fighter)).filter(isModerated=True).all()
    # Берем статистику профессиональных боев
    profi_fights = Fight.objects.filter(Q(fighter_1=fighter) | Q(fighter_2=fighter)).order_by('-event__event_date').all()
    return render(request, 'fighter_details/fighter_details.html', {
        'fighter': fighter,
        'videos': videos,
        'profi_fights': profi_fights,
        'statistic_data': fighter.get_statistic_data()
    })


def fight_details(request, alias):
    # Ищем бой
    fight = get_object_or_404(Fight, alias=alias)
    # Последние 6 боев
    last_6_fights_fighter_1 = Fight.objects.filter(Q(fighter_1=fight.fighter_1) | Q(fighter_2=fight.fighter_1)).exclude(pk=fight.pk).order_by('-id')[:6]
    last_6_fights_fighter_2 = Fight.objects.filter(Q(fighter_1=fight.fighter_2) | Q(fighter_2=fight.fighter_2)).exclude(pk=fight.pk).order_by('-id')[:6]
    # Дополнительные видео с этим боем
    videos = Video.objects.filter(fight=fight, isModerated=True).order_by('-id').all()
    # проверяем оценивался ли этот бой
    cookie = request.COOKIES.get('fight_mark_'+str(fight.id), None)
    client_ip = request.META['REMOTE_ADDR']
    mark = FightHumanRate.objects.filter(ip_address=client_ip, fight=fight).count()
    if cookie is not None or mark != 0:
        mark_exist = True
    else:
        mark_exist = False
    return render(request, 'fight_details/fight_details.html', {
        'fight': fight,
        'last_six_fights_fighter_1': last_6_fights_fighter_1,
        'last_six_fights_fighter_2': last_6_fights_fighter_2,
        'videos': videos,
        'fight_human_rate': fight.get_human_rate(),
        'mark_exist': mark_exist,
    })


def calendar(request, alias=None):
    fights_list = KindOfFight.objects.order_by('name').all()
    events = Event.objects

    if alias is None:
        title = u'Календарь событий'
    else:
        kind_of_fight = get_object_or_404(KindOfFight, alias=alias)
        title = kind_of_fight.name
        events = events.filter(fight__kind_of_fight__alias=alias)

    fight_filter_form = FightsFilterForm(request.GET)
    if not request.GET:
        events = events.filter(event_date__gte=datetime.now().date()).order_by('event_date')
    else:
        events = fight_filter_form.apply_filter_to_event_query_set(events)
    events = events.distinct()
    return render(request, 'calendar/calendar.html', {
        'title': title,
        'events': events,
        'fights_list': fights_list,
        'alias': alias,
        'fight_filter_form': fight_filter_form,
    })


def event_details(request, alias):
    # Ищем событие
    event = get_object_or_404(Event, alias=alias)
    # Получаем дополнительные видео где есть эти бои или бойцы
    fights = event.fight_set.all()
    videos_set = []
    videos = Video.objects.filter(event=event, isModerated=True).order_by('-id')
    videos_set = list(chain(videos_set, videos))
    for fight in fights:
        videos = Video.objects.filter(Q(fight=fight) | Q(fighter_1=fight.fighter_1) | Q(fighter_1=fight.fighter_2) | Q(fighter_2=fight.fighter_1) | Q(fighter_2=fight.fighter_2)).filter(isModerated=True).order_by('-id')
        videos_set = list(chain(videos_set, videos))
    # отправляем в шаблон
    return render(request, 'event_details/event_details.html', {
        'event': event,
        'videos': set(videos_set)
    })


def fights(request, alias=None):
    limit = 50
    page = int(request.GET.get('page', 1))
    show_results = request.GET.get('show_results', False)
    fights_list = KindOfFight.objects.order_by('name').all()
    fights = Fight.objects
    if alias is None:
        title = u'Бои'
    else:
        kind_of_fight = get_object_or_404(KindOfFight, alias=alias)
        title = kind_of_fight.name
        fights = fights.filter(kind_of_fight=kind_of_fight)

    fight_filter_form = FightsFilterForm(request.GET)
    fights = fight_filter_form.apply_filter_to_query_set(fights)
    count_items = len(fights)
    start = page * limit - limit
    end = start + limit
    prev_page = page - 1
    next_page = page + 1

    if count_items % limit == 0:
        count_pages = count_items // limit
    else:
        count_pages = count_items // limit + 1

    return render(request, 'fights/fights.html', {
        'title': title,
        'fights': fights[start:end],
        'fight_list': fights_list,
        'alias': alias,
        'limit': limit,
        'page': page,
        'count_pages': count_pages,
        'prev_page': prev_page,
        'next_page': next_page,
        'range_of_pages': range(1, count_pages + 1),
        'fight_filter_form': fight_filter_form,
        'show_results': show_results
    })


@csrf_exempt
def set_human_mark(request):
    fight_id = request.POST.get('fight_id', None)
    point_in_percents = request.POST.get('mark', 0)
    fight = get_object_or_404(Fight, id=fight_id)
    client_ip = request.META['REMOTE_ADDR']
    point = round(float(point_in_percents)/20, 2)
    new_mark = FightHumanRate(ip_address=client_ip, point_in_percents=round(float(point_in_percents), 2), point=point, fight=fight)
    new_mark.save()
    response = JsonResponse(fight.get_human_rate())
    response.set_cookie('fight_mark_'+str(fight.id), 1, domain=SESSION_DOMAIN)
    return response


@csrf_exempt
def get_human_mark(request):
    fight_id = request.POST.get('fight_id', None)
    fight = get_object_or_404(Fight, id=fight_id)
    response = JsonResponse(fight.get_human_rate())
    return response
