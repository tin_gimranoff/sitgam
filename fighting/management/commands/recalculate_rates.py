import math
from django.core.management.base import BaseCommand
from django.db.models import Count

from fighting.models import FightStarRate

# */15 * * * * cd /home/sitgam/sitgam && /home/sitgam/sitgam/env/bin/python /home/sitgam/sitgam/manage.py recalculate_rates recalculate_star > /dev/null 2>&1

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('fun', type=str)

    def handle(self, *args, **options):
        if options['fun'] == 'recalculate_star':
            self.recalculate_star()

    @staticmethod
    def recalculate_star():
        kind_of_fights = FightStarRate.objects.values('kind_of_fight').annotate(kind_of_fight_count=Count('kind_of_fight'))
        for kind_of_fight in kind_of_fights:
            rates = FightStarRate.objects.filter(kind_of_fight=kind_of_fight['kind_of_fight']).order_by('-points').all()
            sum = 0
            for r in rates:
                sum += r.points
            N = math.ceil(sum / 50)
            star_point = 5
            for i, rate in enumerate(rates):
                rate.position = i + 1
                rate.star_score = star_point
                rate.save()
                if i + 3 > N and (i + 3) % N == 0:
                    star_point -= 0.1
