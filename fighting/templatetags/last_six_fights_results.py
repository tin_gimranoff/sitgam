from django import template
from django.db.models import Q

from fighting.models import Fight
from sitgam.settings import MEDIA_URL

register = template.Library()


@register.inclusion_tag('small_last_fights_winget.html')
def last_six_fights_results(fighter, fight=None):
    fights = Fight.objects.filter(Q(fighter_1=fighter) | Q(fighter_2=fighter))
    if fight:
        fights = fights.filter(Q(event__event_date__lt=fight.event.event_date) | Q(event__event_date=fight.event.event_date, time__lt=fight.time))
    fights = fights.order_by('-event__event_date')[:6]
    return {
        'fights': fights,
        'fighter': fighter,
        'MEDIA_URL': MEDIA_URL,
    }
