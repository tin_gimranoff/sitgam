from django import template
from django.utils.safestring import mark_safe

from settings.models import KindOfFight, WeightCategory
from fighting.models import FightStarRate
from sitgam.settings import MEDIA_URL

register = template.Library()


@register.filter
def get_fighter_statistic_by_sport(fighter, sport):
    data = fighter.get_statistic_data_by_sport(sport)
    return str(data['wins'])+' - '+str(+data['loose'])+' - '+str(data['tie'])


@register.filter
def get_human_rate_for_fighter(fighter, sport):
    rate = fighter.get_human_rate(kind_of_fight_name=sport)
    return mark_safe("%s / <span>%s чел</span>" % (rate['point'], rate['count']))


@register.inclusion_tag('right_panel/_fighter_rate.html')
def rp_fighter_rate():
    sport = KindOfFight.objects.order_by('name').first()
    fighter_rate = FightStarRate.get_rp_fighters_rate(sport)
    return {
        'fighter_rate': enumerate(fighter_rate),
        'MEDIA_URL': MEDIA_URL,
        'sport': sport,
    }


@register.simple_tag()
def rp_weight_categories():
    weight_categories = WeightCategory.objects.order_by('name').all()
    html = ''
    for weight_category in weight_categories:
        html += "<li data-id='%s'><a href='#'>%s</a></li>" % (weight_category.id, weight_category.name)
    return mark_safe(html)
