from django import template

register = template.Library()


@register.filter
def percents(all, part):
    if all == 0 or part == 0:
        return 0
    return int(part / all * 100)
