from ckeditor_demo.settings import MEDIA_URL
from django import template
from django.utils.safestring import mark_safe

from fighting.models import Event
from settings.models import KindOfFight

register = template.Library()


@register.filter
def get_human_rate_for_event(event, sport=None):
    rate = event.get_human_rate(kind_of_fight_name=sport)
    return mark_safe("%s / <span>%s чел</span>" % (rate['point'], rate['count']))


@register.filter
def get_star_rate_for_event(event, sport=None):
    rate = event.get_star_rate(kind_of_fight_name=sport)
    return rate


@register.inclusion_tag('right_panel/_best_events.html')
def rp_best_events():
    sport = KindOfFight.objects.order_by('name').first()
    events = Event.get_rp_best_events(sport)
    return {
        'events': events,
        'MEDIA_URL': MEDIA_URL,
        'sport': sport,
    }
