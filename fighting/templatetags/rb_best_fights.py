from django import template

from fighting.models import Fight
from settings.models import KindOfFight

register = template.Library()


@register.inclusion_tag('right_panel/_best_fights.html')
def rp_best_fights():
    sport = KindOfFight.objects.order_by('name').first()
    fights = Fight.get_rp_best_fights(sport)
    return {
            'fights': enumerate(fights),
    }
