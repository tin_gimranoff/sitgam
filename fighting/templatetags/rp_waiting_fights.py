from django import template

from fighting.models import Fight
from settings.models import KindOfFight

from sitgam.settings import MEDIA_URL

register = template.Library()


@register.inclusion_tag('right_panel/_waiting_fights.html')
def rp_waiting_fights():
    sport = KindOfFight.objects.order_by('name').first()
    fights = Fight.get_rp_wating_fights(sport)
    return {
        'MEDIA_URL': MEDIA_URL,
        'fights': fights,
    }
