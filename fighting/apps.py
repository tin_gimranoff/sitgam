from django.apps import AppConfig


class FightingConfig(AppConfig):
    name = 'fighting'
    verbose_name = u'Бойцы/Бои/События'
