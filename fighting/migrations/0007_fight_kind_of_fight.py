# Generated by Django 2.2 on 2019-04-15 08:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('settings', '0002_stand'),
        ('fighting', '0006_fight_alias'),
    ]

    operations = [
        migrations.AddField(
            model_name='fight',
            name='kind_of_fight',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='settings.KindOfFight', verbose_name='Вид бового искусства'),
        ),
    ]
