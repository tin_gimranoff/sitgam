# Generated by Django 2.2 on 2019-08-18 19:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fighting', '0045_auto_20190713_1457'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fight',
            name='time',
            field=models.TimeField(default='06:00', help_text='Дата берется из события', verbose_name='Время боя'),
        ),
    ]
