# Generated by Django 2.2 on 2019-04-15 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fighting', '0009_fight_tie'),
    ]

    operations = [
        migrations.AddField(
            model_name='fight',
            name='main_video',
            field=models.CharField(blank=True, help_text='Вы можете добавить дополнительные видо в модуле - видео', max_length=255, null=True, verbose_name='Основное видео боя'),
        ),
    ]
