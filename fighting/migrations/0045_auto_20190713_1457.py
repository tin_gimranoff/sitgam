# Generated by Django 2.2 on 2019-07-13 11:57

from django.db import migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('fighting', '0044_auto_20190710_0033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='image',
            field=easy_thumbnails.fields.ThumbnailerImageField(blank=True, help_text='Масштабирование автоматическое', null=True, upload_to='event/', verbose_name='Изображение (138х205)'),
        ),
    ]
