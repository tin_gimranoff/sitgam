from django import template
from textpages.models import Textpage

register = template.Library()


@register.inclusion_tag('__foooter_textpages_menu.html')
def get_footer_textpages_menu():
    items = Textpage.objects.filter(show_in_bottom_menu=True).order_by('name')
    return {
        'items': items,
    }
