from django.apps import AppConfig


class TextpagesConfig(AppConfig):
    name = 'textpages'
    verbose_name = u'Текстовые страницы'
