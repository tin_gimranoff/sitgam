from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from settings.models import SlugModel

class Textpage(SlugModel):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'textpages'
        verbose_name = u'Страница'
        verbose_name_plural = u'Страницы'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название вида')
    body = RichTextUploadingField(verbose_name=u'Тело страницы')
    show_in_bottom_menu = models.BooleanField(verbose_name=u'Отображать в нижнем меню', default=False, blank=False)
