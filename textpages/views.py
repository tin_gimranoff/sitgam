from django.shortcuts import get_object_or_404, render

from textpages.models import Textpage


def show_content(request, alias):
    page = get_object_or_404(Textpage, alias=alias)
    return render(request, 'textpage.html', {
        'page': page,
    })
