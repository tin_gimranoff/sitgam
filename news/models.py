from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField

from fighting.models import Fighter, Event
from settings.models import SlugModel, Promotion, KindOfFight


class Tag(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'tags'
        verbose_name = u'Тег'
        verbose_name_plural = u'Тег'

    name = models.CharField(max_length=255, verbose_name=u'Тег', blank=False, null=False)


class News(SlugModel):
    IMPORTANT_CHOISES = (
        (str(i), str(i)) for i in range(1, 6)
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'news'
        verbose_name = u'Новость'
        verbose_name_plural = u'Новость'

    published_date = models.DateTimeField(verbose_name=u'Дата публикации', blank=False, null=False)
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)
    name = models.CharField(max_length=255, verbose_name=u'Заголовок новости', blank=False, null=False)
    important = models.CharField(max_length=1, choices=IMPORTANT_CHOISES, verbose_name=u'Важность', default=1)
    intro = models.CharField(max_length=255, verbose_name=u'Краткое содержание для главной', blank=True, null=True)
    body = RichTextUploadingField(verbose_name=u'Тело новости')
    kind_of_sport = models.ForeignKey(KindOfFight, verbose_name=u'Вид спорта', null=True, blank=True, on_delete=models.SET_NULL)
    tags = models.ManyToManyField(Tag, verbose_name=u'Набор тегов', blank=True)
    fighters = models.ManyToManyField(Fighter, verbose_name=u'Бойцы', blank=True)
    events = models.ManyToManyField(Event, verbose_name=u'События', blank=True)
    promotions = models.ManyToManyField(Promotion, verbose_name=u'Промоутеры', blank=True)
    news_cover = ThumbnailerImageField(upload_to='news/', blank=False, null=False, verbose_name=u'Обложка (215х175)', resize_source=dict(size=(215, 175), crop=True))
    count_views = models.IntegerField(verbose_name=u'Количество просмотров', default=0)
    tags_string = models.TextField(verbose_name=u'Строка тегов', blank=True, null=True)

    def news_tags(self, tags=None, fighters=None, events=None, promotions=None):
        if tags is None:
            tags = self.tags.get_queryset()
        if fighters is None:
            fighters = self.fighters.get_queryset()
        if events is None:
            events = self.events.get_queryset()
        if promotions is None:
            promotions = self.promotions.get_queryset()
        tag_list = [tag.name for tag in tags]
        tag_list += [fighter.name for fighter in fighters]
        for event in events:
            tag_list.append(event.name)
            tag_list += [promotion.name for promotion in event.promotion.get_queryset()]
        tag_list += [promotion.name for promotion in promotions]
        print(tag_list)
        tag_list = list(set(tag_list))
        return tag_list

    def get_news_tags_for_cloud(self):
        # Вычисляем облако тегов
        all_news_for_cloud = self.objects.only('tags_string').all()
        tags_array = []
        # Собираем все что там получилось в один массив
        for obj in all_news_for_cloud:
            tags_array += eval(obj.tags_string)
        # Из уникального списка теперь для каждого слова высчитываем сколько раз оно лежит в общем массиве
        # Заодно создадим некий общий список в котором будем хранить некий словарь с необходимыми данными для всего
        tags_result_list = []
        for tag in set(tags_array):
            tags_result_list.append({
                'tag': tag,
                'count': tags_array.count(tag),
                'font': 18,
                'opacity': 1
            })
        # Сортируем этот список по количеству вхождений и берем первые 30 штук
        tags_result_list = sorted(tags_result_list, key=lambda k: k['count'], reverse=True)[:30]
        # После того как отсортировали отсеили лишнее мы можем узнать максимально популярный тег и его количество
        max_count = tags_result_list[0]['count']
        # Идем по этому списку и теперь всем просчитываем шрифт и прозрачность
        for tag in tags_result_list:
            tag['font'] = str(tag['count'] * 18 / max_count).replace(',', '.')
            tag['opacity'] = str(tag['count'] / max_count).replace(',', '.')

        return tags_result_list
