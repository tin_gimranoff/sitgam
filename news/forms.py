from django import forms


class NewsFilterForm(forms.Form):

    important = forms.CharField(
        widget=forms.TextInput(attrs={
            'id': 'news-rate',
            'autocomplete': 'off',
        }),
        required=False,
    )

    def apply_filter_to_query_set(self, qs):
        qs_with_filter = qs
        # CHECK FOR VALID DATA IN FORM
        if not self.is_valid():
            return qs
        else:
            # GETTING CLEANED DATA
            data = self.cleaned_data

        # FILTER FOR IMPORTANT
        if data['important'] != '':
            important_filter_arr = data['important'].split(',')
            qs_with_filter = qs.filter(important__gte=important_filter_arr[0], important__lte=important_filter_arr[1])

        # RETURN RESULT
        return qs_with_filter
