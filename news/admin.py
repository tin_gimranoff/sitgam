from django.contrib import admin
from easy_select2 import select2_modelform

from news.models import *

NewsForm = select2_modelform(News, attrs={'width': '300px'})


class NewsAdmin(admin.ModelAdmin):
    form = NewsForm

    class Media:
        css = {
            'all': ('css/admin.css',)
        }

    change_form_template = 'admin/news_change_form.html'

    exclude = ('count_views', 'tags_string',)

    def save_model(self, request, obj, form, change):
        cleaned_data = form.cleaned_data
        obj.tags_string = obj.news_tags(cleaned_data['tags'], cleaned_data['fighters'], cleaned_data['events'], cleaned_data['promotions'])
        super(NewsAdmin, self).save_model(request, obj, form, change)


admin.site.register(Tag)
admin.site.register(News, NewsAdmin)
