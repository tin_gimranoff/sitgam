# Generated by Django 2.2 on 2019-05-24 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0007_auto_20190524_2205'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='count_vews',
            field=models.IntegerField(default=0, verbose_name='Количество просмотров'),
        ),
    ]
