import json

from django.shortcuts import render, get_object_or_404

from news.forms import NewsFilterForm
from news.models import News
from settings.models import KindOfFight
from sitgam.private_settings import COMMENT_WIDGET_ID

NEWS_LIMIT_ON_PAGE = 50


def list(request, alias=None, tag=None):
    current_page = int(request.GET.get('page', 1))
    important_filter = request.GET.get('important', '0,5')
    start_item = current_page*NEWS_LIMIT_ON_PAGE-NEWS_LIMIT_ON_PAGE
    end_item = current_page*NEWS_LIMIT_ON_PAGE
    fights_list = KindOfFight.objects.order_by('name').all()
    news_list = News.objects
    if alias is None and tag is None:
        title = u'Новости'
    else:
        if alias is not None:
            kind_of_fight = get_object_or_404(KindOfFight, alias=alias)
            title = u'%s — Новости' % kind_of_fight.name
            news_list = news_list.filter(kind_of_sport=kind_of_fight)
        if tag is not None:
            title = u'Тег: %s — Новости' % tag
            news_list = news_list.filter(tags_string__contains=tag)
    news_filter_form = NewsFilterForm(request.GET)
    news_list = news_filter_form.apply_filter_to_query_set(news_list)
    count_news = news_list.count()
    news_list = news_list.order_by('-published_date')[start_item:end_item]
    if count_news % NEWS_LIMIT_ON_PAGE == 0:
        count_pages = count_news // NEWS_LIMIT_ON_PAGE
    else:
        count_pages = count_news // NEWS_LIMIT_ON_PAGE + 1
    important_filter_arr = important_filter.split(',')

    return render(request, 'list.html', {
        'news_list': news_list,
        'count_pages': count_pages,
        'current_page': current_page,
        'prev_page': current_page-1,
        'next_page': current_page+1,
        'range_of_pages':range(1, count_pages+1),
        'title': title,
        'fight_list': fights_list,
        'alias': alias,
        'news_filter_form': news_filter_form,
        'start_important_filter': important_filter_arr[0],
        'end_important_filter': important_filter_arr[1],
        'tag_list': News.get_news_tags_for_cloud(News),
    })

def details(request, alias):
    news = get_object_or_404(News, alias=alias)
    news.count_views += 1
    news.save()
    return render(request, 'news_details.html', {
        'news': news,
        'news_tags': news.news_tags(),
        'tag_list': News.get_news_tags_for_cloud(News),
        'COMMENT_WIDGET_ID': COMMENT_WIDGET_ID,
    })
