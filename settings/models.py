from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from django.template.defaultfilters import slugify
from transliterate import translit


class SlugModel(models.Model):
    class Meta:
        abstract = True

    alias = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Алиас для URL', help_text=u'Если оставить пустым заполнится автоматически')

    def save(self, add_id_to_end_url=False, *args, **kwargs):
        if self.alias is not None and self.alias != '':
            alias_translit = slugify(translit(str(self.alias), 'ru', reversed=True))
            if self.alias != alias_translit:
                self.alias = alias_translit
        super().save(*args, **kwargs)
        if self.alias is None or self.alias == '':
            try:
                if self.name_eng != None:
                    self.alias = slugify(translit(str(self.name_eng), 'ru', reversed=True))
                else:
                    self.alias = slugify(translit(str(self.name), 'ru', reversed=True))
            except:
                self.alias = slugify(translit(str(self.name), 'ru', reversed=True))
            if add_id_to_end_url:
                self.alias = self.alias + '_' + str(self.pk)
            self.save()


class KindOfFight(SlugModel):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'kind_of_fights'
        verbose_name = u'Вид боевого искусства'
        verbose_name_plural = u'Виды боевых искусств'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название вида')
    color = models.CharField(max_length=255, blank=False, null=False, default='#ff7200', verbose_name=u'Цвет вида')


class WeightCategory(models.Model):
    GENDER_CHOISES = (
        ('male', 'Мужской'),
        ('female', 'Женский'),
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'weight_categories'
        verbose_name = u'Весовая категория'
        verbose_name_plural = u'Весовые категории'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Весовая категория')
    kind_of_fight = models.ForeignKey(KindOfFight, null=True, blank=True, on_delete=models.SET_NULL, verbose_name=u'Спорт')
    gender = models.CharField(max_length=10, choices=GENDER_CHOISES, default='male', verbose_name=u'Пол бойца')


class Country(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'countries'
        verbose_name = u'Страна'
        verbose_name_plural = u'Страны'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название')
    flag = ThumbnailerImageField(upload_to='countries/', blank=False, null=False, verbose_name=u'Изображение флага',
                                 resize_source=dict(size=(16, 11), crop=True))


class Stand(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'stands'
        verbose_name = u'Стойка'
        verbose_name_plural = u'Стойки'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название')


class KindOfFinish(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'kind_of_finishes'
        verbose_name = u'Вид окончания боя'
        verbose_name_plural = u'Виды окончания боя'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название')


class KindOfWin(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'kind_of_wins'
        verbose_name = u'Способ победы'
        verbose_name_plural = u'Способы победы'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название')


class Titles(SlugModel):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'titles'
        verbose_name = u'Титул'
        verbose_name_plural = u'Титулы'

    name = models.CharField(max_length=255, blank=False, null=False, verbose_name=u'Название')
    kind_of_sport = models.ForeignKey(KindOfFight, on_delete=models.SET_NULL, verbose_name=u'Вид спорта', null=True, blank=True)
    weight_category = models.ForeignKey(WeightCategory, on_delete=models.SET_NULL, verbose_name=u'Весовая категория', null=True, blank=True)


class Promotion(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'promotions'
        verbose_name = 'Промоутер'
        verbose_name_plural = 'Промоутеры'

    name = models.CharField(max_length=255, verbose_name=u'Название', blank=False, null=False)
