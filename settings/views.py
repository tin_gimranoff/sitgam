from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from settings.models import WeightCategory


@csrf_exempt
def get_weight_category_by_sport(request):
    sport_id = request.POST.get('sport_id', None)
    if sport_id is None or sport_id == '':
        return JsonResponse({})
    sports = WeightCategory.objects.values('id', 'name').filter(kind_of_fight=sport_id).order_by('name').all()
    print([item for item in sports])
    return JsonResponse([item for item in sports], safe=False)
