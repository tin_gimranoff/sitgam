from django.apps import AppConfig


class SettingsConfig(AppConfig):
    name = 'settings'
    verbose_name = u'Справочники и настройки системы'
