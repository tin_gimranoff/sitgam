from django import template

from settings.models import KindOfFight

register = template.Library()


@register.inclusion_tag('right_panel/__ajax_filter_tabs.html')
def get_ajax_filter_tabs(action, block):
    fights = KindOfFight.objects.order_by('name').all()
    return {
        'fights': enumerate(fights),
        'action': action,
        'block': block,
    }
