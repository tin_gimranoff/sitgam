from django import template

register = template.Library()


@register.filter
def next_val(number):
    return int(number) + 1
