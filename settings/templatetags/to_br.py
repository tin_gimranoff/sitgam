from django import template

register = template.Library()

@register.filter
def to_br(value):
    return value.replace(";","<br />")

@register.filter
def space_to_br(value):
    return value.replace(" ", "<br />")
