from django.contrib import admin
from settings.models import *


class WeightCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'kind_of_fight', 'gender')


admin.site.register(WeightCategory, WeightCategoryAdmin)
admin.site.register(KindOfFight)
admin.site.register(Stand)
admin.site.register(Country)
admin.site.register(KindOfFinish)
admin.site.register(KindOfWin)
admin.site.register(Titles)
admin.site.register(Promotion)