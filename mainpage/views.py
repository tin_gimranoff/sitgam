from datetime import datetime

from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import get_template
from django.views.decorators.csrf import csrf_exempt

from fighting.models import Fight, FightStarRate, Event
from news.models import News
from settings.models import KindOfFight
from sitgam.settings import MEDIA_URL
from video.models import Video

NEWS_LIMIT_ON_PAGE = 10


def index(request):
    fight_list = KindOfFight.objects.order_by('name').all()
    last_fights = Fight.objects.filter(event__event_date__lt=datetime.now()).order_by('-event__event_date')[:9]
    last_videos = Video.objects.order_by('-id')[:2]
    last_news = News.objects.order_by('-published_date')[:NEWS_LIMIT_ON_PAGE]
    count_news = News.objects.count()
    if count_news % NEWS_LIMIT_ON_PAGE == 0:
        count_pages = count_news // NEWS_LIMIT_ON_PAGE
    else:
        count_pages = count_news // NEWS_LIMIT_ON_PAGE + 1
    return render(request, 'index.html', {
        'fight_list': fight_list,
        'last_fights': last_fights,
        'last_videos': last_videos,
        'last_news': last_news,
        'count_news_pages': count_pages,
    })


@csrf_exempt
def get_last_fights(request):
    param = request.POST.get('param', None)

    last_fights = Fight.objects.filter(event__event_date__lt=datetime.now())

    if param != 'all':
        last_fights = last_fights.filter(kind_of_fight__alias=param)

    last_fights = last_fights.order_by('-event__event_date')[:9]

    template = get_template('fights/_last_battle_item_container.html')

    response = ''

    for fight in last_fights:
        response += template.render({
            'fight': fight,
            'MEDIA_URL': MEDIA_URL,
        })

    return JsonResponse({
        'html': response
    })


@csrf_exempt
def get_last_videos(request):
    param = request.POST.get('param', None)

    if param == 'all':
        last_videos = Video.objects.order_by('-id')[:2]
    else:
        last_videos = Video.objects.filter(fight__kind_of_fight__alias=param).order_by('-id')[:2]

    template = get_template('_video_template_frame.html')

    response = ''

    for video in last_videos:
        response += template.render({
            'link': video.link
        })

    return JsonResponse({
        'html': response
    })


@csrf_exempt
def get_last_news(request):
    param = request.POST.get('param', None)
    page = request.POST.get('page', 1)

    start_item = NEWS_LIMIT_ON_PAGE*int(page)-NEWS_LIMIT_ON_PAGE
    end_item = NEWS_LIMIT_ON_PAGE * int(page)

    if param == 'all':
        last_news = News.objects.order_by('-published_date')[start_item:end_item]
        count_news = News.objects.count()
    else:
        count_news = News.objects.filter(kind_of_sport__alias=param).count()
        last_news = News.objects.filter(kind_of_sport__alias=param).order_by('-published_date')[start_item:end_item]

    if count_news % NEWS_LIMIT_ON_PAGE == 0:
        count_pages = count_news // NEWS_LIMIT_ON_PAGE
    else:
        count_pages = count_news // NEWS_LIMIT_ON_PAGE + 1

    template = get_template('_news_item.html')

    response = ''

    for news in last_news:
        response += template.render({
            'news': news,
            'MEDIA_URL': MEDIA_URL,
        })

    pager = get_template("_paginator.html").render({
        'count_pages': count_pages,
    })

    return JsonResponse({
        'html': response,
        'pager': pager
    })


@csrf_exempt
def get_rp_fighter_rate(request):
    sport_alias = request.POST.get('param', None)
    gender = request.POST.get('gender', None)
    weight_category = request.POST.get('weight_category', None)
    if sport_alias is None:
        return JsonResponse({
            'html': '',
        })
    sport = KindOfFight.objects.get(alias=sport_alias)

    if gender is not None and gender != 'undefined':
        fighter_rates = FightStarRate.get_rp_fighters_rate(sport, gender)
    else:
        fighter_rates = FightStarRate.get_rp_fighters_rate(sport)

    if weight_category is not None and weight_category != '':
        new_fighter_rate = []
        for fighter_rate in fighter_rates:
            weight_categories = fighter_rate.fighter.get_weight_categories()
            weight_categories = weight_categories.filter(kind_of_fight=sport, weight_category_id=weight_category).all()
            if len(weight_categories) != 0:
                new_fighter_rate.append(fighter_rate)
        fighter_rates = new_fighter_rate

    template = get_template('right_panel/__fighter_rate_row.html')

    response = ''

    for k, fighter_rate in enumerate(fighter_rates):
        response += template.render({
            'fighter': fighter_rate.fighter,
            'k': k,
            'star_rate': fighter_rate.star_score,
            'star_place': fighter_rate.row_number,
            'sport': sport,
            'MEDIA_URL': MEDIA_URL
        })

    return JsonResponse({
        'html': response,
    })


@csrf_exempt
def get_rp_waiting_fights(request):
    sport_alias = request.POST.get('param', None)
    if sport_alias is None:
        return JsonResponse({
            'html': '',
        })

    sport = KindOfFight.objects.get(alias=sport_alias)
    fights = Fight.get_rp_wating_fights(sport)

    template = get_template('right_panel/__waiting_fights_row.html')
    response = ''

    for fight in fights:
        response += template.render({
            'fight': fight,
            'MEDIA_URL': MEDIA_URL,
        })

    return JsonResponse({
        'html': response,
    })


@csrf_exempt
def get_rp_best_events(request):
    sport_alias = request.POST.get('param', None)
    if sport_alias is None:
        return JsonResponse({
            'html': '',
        })
    sport = KindOfFight.objects.get(alias=sport_alias)
    events = Event.get_rp_best_events(sport)
    template = get_template('calendar/calendar_item.html')
    response = ''

    for event in events:
        response += template.render({
            'event': event,
            'MEDIA_URL': MEDIA_URL,
            'widget': True,
            'sport': sport,
        })
    return JsonResponse({
        'html': response,
    })


@csrf_exempt
def get_rp_best_fights(request):
    sport_alias = request.POST.get('param', None)
    if sport_alias is None:
        return JsonResponse({
            'html': '',
        })
    sport = KindOfFight.objects.get(alias=sport_alias)
    fights = Fight.get_rp_best_fights(sport)

    template = get_template('right_panel/__best_fight_row.html')
    response = ''

    for k,fight in enumerate(fights):
        response += template.render({
            'fight': fight,
            'k': k,
        })
    return JsonResponse({
        'html': response,
    })
